#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from functools import namedtuple
import os
from sys import exc_info
from traceback import print_tb

from flask import Flask, jsonify, make_response
from flask_cors import CORS

from escape.main import init as main_init
from escape.server.main import define_main_route
from escape.server.util import fail

from acatpy.speak import yellow, bright_red
from acatpy.io.speak import warn, info, iwarn
from acatpy.io.speak import set_level as set_speak_level # noqa
from acatpy.io.io import die

set_speak_level('DEBUG')

def get_env_value(key, default=None, convert=str, valid=None, required=True):
    val = os.environ.get(key)
    if not val:
        if default is None and required:
            die('Missing required environment variable %s' % bright_red(key))
        info('%s not set in environment: using %s' % (key, bright_red(default)))
        return default
    val = convert(val)
    if valid and val not in valid:
        es = (yellow(_) for _ in valid)
        warn(
            '%s (%s) not recognised, valid values are (%s), using %s' %
            (key, bright_red(val), '|'.join(es), bright_red(default))
        )
        return default
    return val

def info_env(key, val):
    info('[ %s ] -> %s' % (yellow(key), val))

def init(app):
    CORS(app)

def make_application():
    application = Flask(
        __name__,
        static_url_path='/static',
        static_folder='../web/htdocs',
    )

    @application.errorhandler(404)
    def not_found(_):
        return make_response(jsonify({'error': 'Not found'}), 404)

    @application.errorhandler(Exception)
    def error_handler_general(err):
        # @todo ierror
        the_type, value, traceback = exc_info()
        iwarn("%s\n--\nstacktrace:" % repr(err))
        print_tb(traceback)

        return fail(
            500,
            imsg='Runtime exception (see server logs)',
            abort=False,
        )

    return application

RuntimeConfig = namedtuple('RuntimeConfig', (
    'env',
    'db',
))

# --- @todo
envs = {'tst', 'acc'}
default_env = 'tst'

# --- uwsgi expects this to be called 'application'.
application = make_application()

db = main_init()
env_context = get_env_value('ESCAPE_ENV', default=default_env, valid=envs)
info_env('env-context', env_context)
runtime_config = RuntimeConfig(env_context, db)

define_main_route(application, runtime_config, None)
