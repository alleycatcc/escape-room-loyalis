// ------ this is used by the build process (i.e. yarn/npm run build ...)

defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  tap, concatTo, lets,
  dot2, xMatchStr, prop,
} from 'stick-js/es'

import pathMod from 'path'

import { HashedModuleIdsPlugin, } from 'webpack'

import CompressionPlugin from 'compression-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import OfflinePlugin from 'offline-plugin'
import TerserPlugin from 'terser-webpack-plugin'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import WebpackPwaManifest from 'webpack-pwa-manifest'

import base from './webpack.base.babel'

const DO_OFFLINE_PLUGIN = false

const replace = 'replace' | dot2

const { APP_ENV, } = process.env

const offlinePlugin = () => !DO_OFFLINE_PLUGIN ? [] : [
  new OfflinePlugin ({
    relativePaths: false,
    publicPath: '/',
    appShell: '/',

    excludes: ['.htaccess'],

    caches: {
      main: [':rest:'],

      // --- react-boilerplate
      // All chunks marked as `additional`, loaded after main section
      // and do not prevent SW to install. Change to `optional` if
      // do not want them to be preloaded at all (cached only when first loaded)
      additional: ['*.chunk.js'],
    },

    safeToUseOptionalCaches: true,
  })
]

const plugins = [
  new HtmlWebpackPlugin ({
    template: 'app/index.html',
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true,
    },
    inject: true,
  }),

  // --- react-boilerplate
  // Put it in the end to capture all the HtmlWebpackPlugin's
  // assets manipulations and do leak its manipulations to HtmlWebpackPlugin

  ... offlinePlugin (),

  // --- necessary? the server is set to gzip responses as well; probably doesn't have much effect
  // to do it twice.
  new CompressionPlugin ({
	algorithm: 'gzip',
	test: /\.js$|\.css$|\.html$/,
	threshold: 10240,
	minRatio: 0.8,
  }),

  new HashedModuleIdsPlugin ({
    hashFunction: 'sha256',
    hashDigest: 'hex',
    hashDigestLength: 20,
  }),
]

module.exports = base ({
  plugins,

  // --- automatically configures DefinePlugin and sets NODE_ENV to production.
  mode: 'production',

  entry: [pathMod.join (process.cwd (), 'app/app.js')],

  // --- react-boilerplate
  // use content hash ('chunkhash') instead of compilation hash to improve caching.
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin ({
        terserOptions: {
          warnings: false,
          compress: {
            comparisons: false,
          },
          parse: {},
          mangle: true,
          output: {
            comments: false,
            ascii_only: true,
          },
        },
        parallel: true,
        cache: true,
        sourceMap: true,
      }),
    ],
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: RegExp ('/node_modules/'),
          // --- given ramda/a, ramda/b, ramda, make a chunk called
          // npm.ramda.[hash].chunk.js[.gz]
          name: ({ context, }) => lets (
            () => context | xMatchStr (
              '/node_modules/ (.*?) (/|$)',
            ),
            (packageMatch) => packageMatch | prop (1) | replace ('@', ''),
            (_, x) => x | concatTo ('npm.'),
          ),
        },
      },
    },
  },

  devtool: APP_ENV === 'tst' && 'eval-source-map',

  performance: {
    assetFilter: assetFilename =>
      !/(\.map$)|(^(main\.|favicon\.))/.test (assetFilename),
  },
})
