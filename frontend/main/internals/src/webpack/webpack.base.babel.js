defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  concat,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  mergeTo,
} from 'stick-js/es'

// --- old, but may require tweaking or reviving in the case of things like the passphrases app
// (i.e. a bundle retrieved via file:/// urls, a webview component, etc.)
// const useRelativePath = process.env.WEBPACK_TYPE === 'build'

import pathMod from 'path'

import webpack from 'webpack'

const basePlugins = [
  new webpack.ProvidePlugin ({
    fetch: 'exports-loader?self.fetch!whatwg-fetch',
  }),

  new webpack.DefinePlugin ({
    'process.env': {
      NODE_ENV: JSON.stringify (process.env.NODE_ENV),
      APP_ENV: JSON.stringify (process.env.APP_ENV),
    },
  }),
]

// type: javascript/esm

export default ({
  mode, entry, output, optimization, plugins, devtool, performance={},
  babelOptions={},
}) => ({
  mode,
  entry,
  optimization,
  devtool,
  performance,

  // --- gives us `window` var
  target: 'web',

  output: output | mergeTo ({
    path: pathMod.resolve (process.cwd (), 'build'),
    publicPath: '/',
  }),

  plugins: plugins | concat (basePlugins),

  resolve: {
    // --- @todo consider removing app: we use full relative paths on imports anyway.
    modules: ['node_modules', 'app'],
    extensions: ['.js', '.jsx'],
    mainFields: ['browser', 'jsnext:main', 'main'],
  },

  module: {
    rules: [
      // --- experiment with esm style (could also just call it app.mjs).
      // --- doesn't seem to enforce named imports existing, so not much point at the moment in
      // changing everything to .mjs.
	  // --- also it's difficult or impossible to mix with non-es modules, which is a lot of them.
      /*
      {
        test: /app\.js$/,
        type: 'javascript/esm',
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions,
        },
      },
      */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions,
        },
      },

      // --- these node_modules use 'const', so we transpile them.
      // --- added this to support an ancient version of Safari (which we don't support any more).
      // --- not sure how IE ever worked without this.
      {
        test: /memoize-immutable\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {},
        },
      },
      {
        test: /tuplemap\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {},
        },
      },

      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3|cur)$/,
        use: 'file-loader',
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10 * 1024,
              noquotes: true,
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10 * 1024,
            },
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                enabled: false,

                // --- react-boilerplate:
                // NOTE: mozjpeg is disabled as it causes errors in some Linux environments
                // Try enabling it in your environment by switching the config to:
                // enabled: true,
                // progressive: true,
              },
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
            },
          },
        ],
      },
      {
        type: 'javascript/auto',
        test: /manifest\.json$/,
        exclude: /node_modules/,
        use: [{
          loader: 'file-loader',
          options: { name: '[name].[ext]' },
        }],
      },
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.(mp4|webm)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
          },
        },
      },
    ],
  },
})
