"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _component = _interopRequireDefault(require("./component"));

var _container = _interopRequireDefault(require("./container"));

var _op = function _op() {
  return _stickJs.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _stickJs.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _stickJs.composeRight.apply(void 0, arguments);
};

var setGenerator = (0, _stickJs.side2)('setGenerator');
var addHelper = (0, _stickJs.side2)('addHelper');
var relPath = '../../../app/containers';

var containerPath = _op3((0, _stickJs.concatTo)([__dirname, relPath]), pathJoinN);

var pathJoinN = (0, _stickJs.passToN)(_path["default"].join);
var exists = _fs["default"].existsSync;

var ifExists = _op(exists, _stickJs.ifPredicate);

var getDirectory = function getDirectory(stub) {
  return _op(_op(stub, containerPath), ifExists(function (_) {
    return 'containers/' + stub;
  }, function (_) {
    return 'components/' + stub;
  }));
};

module.exports = function (plop, cfg) {
  return _op(_op(_op(_op(plop, setGenerator('component', _component["default"])), setGenerator('container', _container["default"])), addHelper('directory', getDirectory)), addHelper('curly', function (object, open) {
    return open ? '{' : '}';
  }));
};