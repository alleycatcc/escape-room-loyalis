"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _componentExists = _interopRequireDefault(require("../utils/componentExists"));

var _op = function _op() {
  return _stickJs.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _stickJs.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _stickJs.composeRight.apply(void 0, arguments);
};

var relPath = '../../..';
var appPath = relPath + '/app';
module.exports = {
  description: 'Add a container',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    "default": 'Button',
    validate: function validate(value) {
      return _op(value, (0, _stickJs.condS)([_op(_op('', _stickJs.eq), (0, _stickJs.guardV)('The name is required')), _op(_componentExists["default"], (0, _stickJs.guardV)('A component or container with this name already exists')), _op(_stickJs.otherwise, (0, _stickJs.guard)(_stickJs.T))]));
    }
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    "default": true,
    message: 'Do you want to load resources asynchronously?'
  }],
  actions: function actions(data) {
    var componentTemplate = _op(data.type, (0, _stickJs.condS)([_op(_op('Stateless Function', _stickJs.eq), (0, _stickJs.guardV)('./container/stateless.js.hbs')), _op(_stickJs.otherwise, (0, _stickJs.guardV)('./container/class.js.hbs'))]));

    var actions = [{
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/index.js',
      templateFile: componentTemplate,
      abortOnFail: true
    }];
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/messages.js',
      templateFile: './container/messages.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/actions.js',
      templateFile: './container/actions.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/actions.test.js',
      templateFile: './container/actions.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/constants.js',
      templateFile: './container/constants.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/selectors.js',
      templateFile: './container/selectors.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/selectors.test.js',
      templateFile: './container/selectors.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/reducer.js',
      templateFile: './container/reducer.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/reducer.test.js',
      templateFile: './container/reducer.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/saga.js',
      templateFile: './container/saga.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/saga.test.js',
      templateFile: './container/saga.test.js.hbs',
      abortOnFail: true
    });

    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path: appPath + '/containers/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true
      });
    }

    return actions;
  }
};