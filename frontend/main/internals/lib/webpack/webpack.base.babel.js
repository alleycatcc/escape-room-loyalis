"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _es = require("stick-js/es");

var _path = _interopRequireDefault(require("path"));

var _webpack = _interopRequireDefault(require("webpack"));

var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

var basePlugins = [new _webpack["default"].ProvidePlugin({
  fetch: 'exports-loader?self.fetch!whatwg-fetch'
}), new _webpack["default"].DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    APP_ENV: JSON.stringify(process.env.APP_ENV)
  }
})]; // type: javascript/esm

var _default = function _default(_ref) {
  var mode = _ref.mode,
      entry = _ref.entry,
      output = _ref.output,
      optimization = _ref.optimization,
      plugins = _ref.plugins,
      devtool = _ref.devtool,
      _ref$performance = _ref.performance,
      performance = _ref$performance === void 0 ? {} : _ref$performance,
      _ref$babelOptions = _ref.babelOptions,
      babelOptions = _ref$babelOptions === void 0 ? {} : _ref$babelOptions;
  return {
    mode: mode,
    entry: entry,
    optimization: optimization,
    devtool: devtool,
    performance: performance,
    // --- gives us `window` var
    target: 'web',
    output: _op(output, (0, _es.mergeTo)({
      path: _path["default"].resolve(process.cwd(), 'build'),
      publicPath: '/'
    })),
    plugins: _op(plugins, (0, _es.concat)(basePlugins)),
    resolve: {
      // --- @todo consider removing app: we use full relative paths on imports anyway.
      modules: ['node_modules', 'app'],
      extensions: ['.js', '.jsx'],
      mainFields: ['browser', 'jsnext:main', 'main']
    },
    module: {
      rules: [// --- experiment with esm style (could also just call it app.mjs).
      // --- doesn't seem to enforce named imports existing, so not much point at the moment in
      // changing everything to .mjs.
      // --- also it's difficult or impossible to mix with non-es modules, which is a lot of them.

      /*
      {
        test: /app\.js$/,
        type: 'javascript/esm',
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions,
        },
      },
      */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions
        }
      }, // --- these node_modules use 'const', so we transpile them.
      // --- added this to support an ancient version of Safari (which we don't support any more).
      // --- not sure how IE ever worked without this.
      {
        test: /memoize-immutable\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {}
        }
      }, {
        test: /tuplemap\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {}
        }
      }, {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader']
      }, {
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader']
      }, {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3|cur)$/,
        use: 'file-loader'
      }, {
        test: /\.svg$/,
        use: [{
          loader: 'svg-url-loader',
          options: {
            limit: 10 * 1024,
            noquotes: true
          }
        }]
      }, {
        test: /\.(jpe?g|png|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10 * 1024
          }
        }, {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: {
              enabled: false // --- react-boilerplate:
              // NOTE: mozjpeg is disabled as it causes errors in some Linux environments
              // Try enabling it in your environment by switching the config to:
              // enabled: true,
              // progressive: true,

            },
            gifsicle: {
              interlaced: false
            },
            optipng: {
              optimizationLevel: 7
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            }
          }
        }]
      }, {
        type: 'javascript/auto',
        test: /manifest\.json$/,
        exclude: /node_modules/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]'
          }
        }]
      }, {
        test: /\.html$/,
        use: 'html-loader'
      }, {
        test: /\.(mp4|webm)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }
      }]
    }
  };
};

exports["default"] = _default;