"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _es = require("stick-js/es");

var _path = _interopRequireDefault(require("path"));

var _webpack = require("webpack");

var _compressionWebpackPlugin = _interopRequireDefault(require("compression-webpack-plugin"));

var _htmlWebpackPlugin = _interopRequireDefault(require("html-webpack-plugin"));

var _offlinePlugin = _interopRequireDefault(require("offline-plugin"));

var _terserWebpackPlugin = _interopRequireDefault(require("terser-webpack-plugin"));

var _uglifyjsWebpackPlugin = _interopRequireDefault(require("uglifyjs-webpack-plugin"));

var _webpackPwaManifest = _interopRequireDefault(require("webpack-pwa-manifest"));

var _webpackBase = _interopRequireDefault(require("./webpack.base.babel"));

// ------ this is used by the build process (i.e. yarn/npm run build ...)
var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

var DO_OFFLINE_PLUGIN = false;

var replace = _op('replace', _es.dot2);

var APP_ENV = process.env.APP_ENV;

var offlinePlugin = function offlinePlugin() {
  return !DO_OFFLINE_PLUGIN ? [] : [new _offlinePlugin["default"]({
    relativePaths: false,
    publicPath: '/',
    appShell: '/',
    excludes: ['.htaccess'],
    caches: {
      main: [':rest:'],
      // --- react-boilerplate
      // All chunks marked as `additional`, loaded after main section
      // and do not prevent SW to install. Change to `optional` if
      // do not want them to be preloaded at all (cached only when first loaded)
      additional: ['*.chunk.js']
    },
    safeToUseOptionalCaches: true
  })];
};

var plugins = [new _htmlWebpackPlugin["default"]({
  template: 'app/index.html',
  minify: {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: true
  },
  inject: true
})].concat((0, _toConsumableArray2["default"])(offlinePlugin()), [// --- necessary? the server is set to gzip responses as well; probably doesn't have much effect
// to do it twice.
new _compressionWebpackPlugin["default"]({
  algorithm: 'gzip',
  test: /\.js$|\.css$|\.html$/,
  threshold: 10240,
  minRatio: 0.8
}), new _webpack.HashedModuleIdsPlugin({
  hashFunction: 'sha256',
  hashDigest: 'hex',
  hashDigestLength: 20
})]);
module.exports = (0, _webpackBase["default"])({
  plugins: plugins,
  // --- automatically configures DefinePlugin and sets NODE_ENV to production.
  mode: 'production',
  entry: [_path["default"].join(process.cwd(), 'app/app.js')],
  // --- react-boilerplate
  // use content hash ('chunkhash') instead of compilation hash to improve caching.
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js'
  },
  optimization: {
    minimize: true,
    minimizer: [new _terserWebpackPlugin["default"]({
      terserOptions: {
        warnings: false,
        compress: {
          comparisons: false
        },
        parse: {},
        mangle: true,
        output: {
          comments: false,
          ascii_only: true
        }
      },
      parallel: true,
      cache: true,
      sourceMap: true
    })],
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: RegExp('/node_modules/'),
          // --- given ramda/a, ramda/b, ramda, make a chunk called
          // npm.ramda.[hash].chunk.js[.gz]
          name: function name(_ref) {
            var context = _ref.context;
            return (0, _es.lets)(function () {
              return _op(context, (0, _es.xMatchStr)('/node_modules/ (.*?) (/|$)'));
            }, function (packageMatch) {
              return _op(_op(packageMatch, (0, _es.prop)(1)), replace('@', ''));
            }, function (_, x) {
              return _op(x, (0, _es.concatTo)('npm.'));
            });
          }
        }
      }
    }
  },
  devtool: APP_ENV === 'tst' && 'eval-source-map',
  performance: {
    assetFilter: function assetFilter(assetFilename) {
      return !/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename);
    }
  }
});