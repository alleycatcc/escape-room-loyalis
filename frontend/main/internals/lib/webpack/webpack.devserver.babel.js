"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _es = require("stick-js/es");

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _glob = _interopRequireDefault(require("glob"));

var _webpack = _interopRequireDefault(require("webpack"));

var _circularDependencyPlugin = _interopRequireDefault(require("circular-dependency-plugin"));

var _htmlWebpackPlugin = _interopRequireDefault(require("html-webpack-plugin"));

var _webpackBase = _interopRequireDefault(require("./webpack.base.babel"));

// ------ this is used by the dev server (i.e. yarn/npm run start)
var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

var plugins = [new _webpack["default"].HotModuleReplacementPlugin(), new _htmlWebpackPlugin["default"]({
  inject: true,
  template: 'app/index.html'
}), new _circularDependencyPlugin["default"]({
  // --- a.js: it seems you can name a module e.g. fooa.js to bypass this check if necessary.
  exclude: /a\.js|node_modules/,
  // --- only warn
  failOnError: false
})];
module.exports = (0, _webpackBase["default"])({
  plugins: plugins,
  mode: 'development',
  entry: [// --- @todo not sure this is necessary -- we are also loading polyfills in app.js, and we are
  // not doing this step in the webpack.prod config.
  require.resolve('react-app-polyfill/ie11'), 'webpack-hot-middleware/client?reload=true', _path["default"].join(process.cwd(), 'app/app.js')],
  // --- react-boilerplate:
  // Don't use hashes in dev mode for better performance
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  devtool: 'eval-source-map',
  performance: {
    hints: false
  }
});