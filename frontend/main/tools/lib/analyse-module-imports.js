#!/usr/bin/env node
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _fs = _interopRequireDefault(require("fs"));

var _es = require("stick-js/es");

var _fishLib = _interopRequireWildcard(require("fish-lib"));

var _nodeSourceWalk = _interopRequireDefault(require("node-source-walk"));

var _util = _interopRequireDefault(require("util"));

var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

(0, _fishLib.bulletSet)({
  type: 'star'
});

var inspect = function inspect(x) {
  return _util["default"].inspect(x, {
    depth: null,
    colors: process.stdout.isTTY
  });
}; // --- dies


var getFilenamesCmd = function getFilenamesCmd() {
  return (0, _fishLib.sysSpawn)('find', [__dirname + '/../../app', '-name', '*.js'], {
    sync: true,
    outSplit: true
  });
};

var getFilenames = _op3(getFilenamesCmd, (0, _es.prop)('out'));

var getSource = function getSource(path) {
  return _fs["default"].readFileSync(path).toString();
};

var underline = (0, _es.sprintf1)("\x1B[4m%s\x1B[24m");

var indent = function indent(n) {
  return function (s) {
    return _op(_op(_op(n, (0, _es.repeatV)('  ')), (0, _es.appendM)(s)), (0, _es.join)(''));
  };
};

var onEntry = _op3(_fishLib.green, _fishLib.info);

var onImport = _op3(_op3(underline, indent(1)), _fishLib.log);

var onDefaultImport = function onDefaultImport(s) {
  return _op(_op(_op([_op('default', _fishLib.brightRed), s], (0, _es.sprintfN)('%s as %s')), indent(2)), _fishLib.log);
};

var onNamedImport = function onNamedImport(_ref) {
  var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
      local = _ref2[0],
      imported = _ref2[1];

  return _op(_op(local, (0, _es.eq)(imported)), (0, _es.ifYes)(function () {
    return _op(_op(imported, indent(2)), _fishLib.log);
  }, function () {
    return _op(_op(_op([imported, local], (0, _es.sprintfN)('%s as %s')), indent(2)), _fishLib.log);
  }));
};

var makeWalker = _op(_nodeSourceWalk["default"], _es.neu);

var walk = _op('walk', _es.dot2);

var walkSource = function walkSource(source) {
  return _op(makeWalker, walk(source, (0, _es.deconstruct2)(function (_ref3) {
    var source = _ref3.source,
        imported = _ref3.imported,
        type = _ref3.type,
        local = _ref3.local;
    return function (node) {
      // return node | inspect | log
      _op(type, (0, _es.condS)([_op(_op('ImportDeclaration', _es.eq), (0, _es.guard)(function () {
        return _op(node, (0, _es.deconstruct)(function (_ref4) {
          var value = _ref4.source.value;
          return _op(value, onImport);
        }));
      })), _op(_op('ImportDefaultSpecifier', _es.eq), (0, _es.guard)(function () {
        return _op(node, (0, _es.deconstruct)(function (_ref5) {
          var name = _ref5.local.name;
          return _op(name, onDefaultImport);
        }));
      })), _op(_op('ImportSpecifier', _es.eq), (0, _es.guard)(function () {
        return _op(node, (0, _es.deconstruct)(function (_ref6) {
          var local = _ref6.local,
              imported = _ref6.imported;
          return _op(_op([local, imported], (0, _es.map)((0, _es.prop)('name'))), onNamedImport);
        }));
      }))]));
    };
  })));
};

var walkFile = function walkFile(entry) {
  return _op(_op(entry, getSource), walkSource);
};

var go = function go(entries) {
  return _op(entries, (0, _es.map)(function (entry) {
    return _op(_op(entry, (0, _es.tap)(onEntry)), walkFile);
  }));
};

go(getFilenames());