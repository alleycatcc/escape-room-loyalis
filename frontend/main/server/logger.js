const chalk = require ('chalk')
const ip = require ('ip')

module.exports = logger = {

  error: (err) => {
    console.error (chalk.red (err))
  },

  appStarted: (port, host, ssl) => {
    console.log (`Server started! ${chalk.green ('✓')}`)
    const protocol = ssl ? 'https' : 'http'

    console.log (`
Localhost: ${chalk.magenta (`${protocol}://${host}:${port}`)}
      LAN: ${chalk.magenta (`${protocol}://${ip.address ()}:${port}`)}\n`)
  },
}
