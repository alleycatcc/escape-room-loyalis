// --- not passed through babel: i.e. const is ok, but not import.

const path = require ('path')
const webpack = require ('webpack')
const webpackDevMiddleware = require ('webpack-dev-middleware')
const webpackHotMiddleware = require ('webpack-hot-middleware')

const proxy = require ('http-proxy-middleware')

const createWebpackMiddleware = (compiler, publicPath) =>
  webpackDevMiddleware (compiler, {
    publicPath,
    noInfo: true,
    silent: true,
    stats: 'errors-only',
  })

module.exports = function addDevMiddlewares (app, webpackConfig) {
  const compiler = webpack (webpackConfig)
  const middleware = createWebpackMiddleware (compiler, webpackConfig.output.publicPath)

  app.use (middleware)
  app.use (webpackHotMiddleware (compiler))

  app.use ('/api', proxy ({
    target: 'http://localhost:8081',
    logLevel: 'debug',
    changeOrigin: true,
    // --- let nginx do it.
    // pathRewrite: { '^/api': '' },
  }))

  // --- webpackDevMiddleware uses memory-fs to store build artifacts.
  const fs = middleware.fileSystem

  app.get ('*', (req, res) => {
    fs.readFile (path.join (compiler.outputPath, 'index.html'),
      (err, file) => {
        if (err) res.sendStatus (404)
        else res.send (file.toString ())
      }
    )
  })
}
