process.env.APP_ENV = process.env.APP_ENV || 'tst'

const fs = require ('fs')
const https = require ('https')

const express = require ('express')
const logger = require ('./logger')
const setup = require ('./middlewares/frontendMiddleware')

const app = express ()
setup (app)

const { SSL: useSSL, HOST: host=null, PORT: portStr=3000, } = process.env
const port = Number (portStr)
const prettyHost = host || 'localhost'

// --- not sure if proxying will work with ssl though.
const getServer = (ssl) => {
  if (useSSL) {
    const key = fs.readFileSync (__dirname + '/key.pem')
    const cert = fs.readFileSync (__dirname + '/cert.pem')

    /*
     openssl genrsa -out key.pem 4096
     openssl req -new -key key.pem -out certrequest.csr
     openssl x509 -req -in certrequest.csr -signkey key.pem -out cert.pem
    */

    const options = {
      key,
      cert,
    }
    return https.createServer (options, app)
  }
  return app
}

getServer (useSSL).listen (port, host, (err) => {
  if (err)
    return logger.error (err.message)

  logger.appStarted (port, prettyHost, useSSL)
})
