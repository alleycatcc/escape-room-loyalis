defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  againstBoth, ne, not, eq,
  concatTo, lt, bindProp, join, sprintf1,
  noop, bindTryProp, defaultTo,
  lets, invoke,
  T, F,
  prop,
  recurry,
  mergeM, tap, whenOk,
  ifPredicate,
  list,
  defaultToV,
  last,
  die,
  dot1,
  dot4,
  path,
  whenPredicateV,
  passToN, deconstruct,
  ok,
} from 'stick-js/es'

import { put as sagaPut, take as sagaTake, } from 'redux-saga/effects'
import rfdc from 'rfdc'

import { recover, } from 'alleycat-js/es/async'
import { toJust, } from 'alleycat-js/es/bilby'
import { logWith, getQueryParams, isNotEmptyList, } from 'alleycat-js/es/general'
import { letsP, } from 'alleycat-js/es/letsPromise'
import { all, allV, } from 'alleycat-js/es/predicate'
import { deconstructProps, withDisplayName, } from 'alleycat-js/es/react'

import {
  mlt, mgt, mediaRule,
} from 'alleycat-js/es/styled'

const cloneDeep = rfdc ()

// --- @todo alleycat-js toJust is a bad name.
export const fromJust = toJust

export const fold4 = 'fold' | dot4

export const appEnv = process.env.APP_ENV || 'blah'
export const envIsNotPrdLike = appEnv | againstBoth (ne ('acc'), ne ('prd'))
export const envIsPrdLike = envIsNotPrdLike | not
export const envIsPrd = appEnv | eq ('prd')
export const envIsNotPrd = envIsPrd | not
export const envIsAcc = appEnv | eq ('acc')
export const envIsTst = appEnv | eq ('tst')
export const envIsDev = appEnv | eq ('dev')
export const envIsNotTst = envIsTst | not
export const envIsTstLike = envIsTst || envIsDev
export const envIsNotTstLike = envIsTstLike | not

export const log = console | bindProp ('log')
export const debug = console | bindTryProp ('debug') | defaultTo (() => log)

export const debugIt = (...args) => debug ('[debug] *', ...args)
export const debugDev = envIsDev ? debugIt : noop
export const debugDevWith = header => (...args) => debugDev (... [header, ...args])

; `
These are based on the Bootstrap 4 breakpoints.

Note that these are from the given width *and up*; in other words, mediaTablet is tablets and all
desktops, etc.

For GrNe we generally need mediaPhone, mediaTablet, and mediaDesktop.
`

const mediaPhoneWidth      = 0
const mediaPhoneBigWidth   = 576
const mediaTabletWidth     = 768
const mediaDesktopWidth    = 992

// --- For escape-room-loyalis we're changing this from the bootstrap value.
const mediaDesktopBigWidth = 1700 // bootstrap = 1500

export const mediaPhone      = mediaPhoneWidth      | mgt | mediaRule
export const mediaPhoneBig   = mediaPhoneBigWidth   | mgt | mediaRule
export const mediaTablet     = mediaTabletWidth     | mgt | mediaRule
export const mediaDesktop    = mediaDesktopWidth    | mgt | mediaRule
export const mediaDesktopBig = mediaDesktopBigWidth | mgt | mediaRule

export const isMobileWidth = mediaTabletWidth | lt

export const scrollIntoViewSmooth = (opts={}) => (elem) => elem.scrollIntoView ({
  behavior: 'smooth',
  block: 'center',
  ...opts,
})

export const scrollIntoViewAuto = (opts={}) => (elem) => elem.scrollIntoView ({
  behavior: 'auto',
  block: 'center',
  ...opts,
})

export const browseLink = String >> concatTo ('/browse/')
export const browseLinkForRouter = String >> concatTo ('/browse/')

export const browseToId = recurry (2) (
  (history) => (id) => browseTo (history) (id | browseLink)
)

export const browseTo = recurry (2) (
  (history) => (to) => history.push (to),
)

window | mergeM ({
  browseToId, browseTo,
})

// --- note, user agent tests are never totally reliable.
// --- see mdn docs for a pretty good break-down.

export const isSafari = () => lets (
  () => window.navigator.userAgent,
  (ua) => allV (
    ua.indexOf ('Safari') !== -1,
    ua.indexOf ('Chrome') === -1,
    ua.indexOf ('Chromium') === -1,
  ),
)

/* Momentum scroll seems to be causing problems (flickering, bouncing weird & possibly causing other
 * problems).
 * Seen on iPhone 5 (iOS11 / 605) and iPhone SE.
 * For now, disabling on all (mobile) Safaris.
 */
export const shouldDisableMomentumScroll = invoke (() => {
  const cutoff = Infinity

  const q = getQueryParams ()
  if (q.momentum === '1') return F

  return () => lets (
    () => window.navigator.userAgent,
    (ua) => all (
      () => isSafari (),
      () => ua.match (/AppleWebKit\/(\d+)/),
      (_, m) => m | prop (1) | Number | lt (cutoff),
    ),
  )
})

/*
 * Safari on iPhone is giving a lot of problems related to single/abridge mode.
 * This is a stop-gap measure to just disable single/abridge on mobile and (effectively) always use
 * BrowseModeLemmataExplore.
 */

export const disableSingle = getQueryParams >> prop ('disableSingle') >> eq ('1')

export const onKruimelItemClickStandard = (history) => (link) =>
  () => browseTo (history, link)

export const conformsTo = (shape) => (o) => {
  for (const k in shape) {
    const p = shape [k]
    if (!p (o [k])) return false
  }
  return true
}

export const arrow = recurry (4) (
  (f) => (g) => (final) => (x) =>
    final (x | f, x | g),
)

export const untag = 'untag' | dot1

const equalsOne = 1 | eq
const ifEqualsOne = equalsOne | ifPredicate

export const loy = ifEqualsOne (
  () => 'Loy',
  () => 'Loy',
)

export const scoreAmountList = arrow (
  String,
  loy,
  list,
)

export const scoreAmount = scoreAmountList >> join (' ')

// --- @todo alleycat-js
// --- `component` sets the 'displayName' property, which is handy for errors.
// --- note that eslint will still complain about 'missing display name'
export const component = recurry (2) (
  (name) => (f) => withDisplayName (name) (
    deconstructProps (f),
  ),
)

export const init = (xs) => {
  const ret = []
  const l = xs.length
  for (let n = 0; n < l - 1; n++)
    ret.push (xs [n])
  return ret
}

export const chop = arrow (init, last, list)

// --- @todo alleycat-js / stick-js
// --- @todo not clear what should happen if objects & lists are mixed.

/* Immutably set a path in a list of lists.
 *
 * e.g.
 *
 *     const xs = [
 *       [[1, 2, 3], 2, 3],
 *       [[4, 5, 6], 5, 6],
 *     ]
 *     xs | assocPathList ([0, 0, 2], 10)
 *
 * yields
 *
 *     [
 *       [[1, 2, 10], 2, 3],
 *       [[4, 5, 6], 5, 6],
 *     ]
 *
 * Will deep clone the list before doing anything.
 */

export const assocPathList = recurry (3) (
  (path) => (val) => (xs) => {
    const vs = xs | cloneDeep
    let ptr = vs
    const [ys, y] = path | chop
    for (const z of ys) {
      ptr [z] = ptr [z] || []
      ptr = ptr [z]
    }
    ptr [y] = val
    return vs
  },
)

export const assocList = recurry (3) (
  (idx) => (val) => (xs) => assocPathList ([idx], val, xs)
)

// --- `orElse` runs the given function as a side-effect; for the non-side-effect version, use
// `defaultTo(V)`

export const orElse = tap << defaultTo
export const orDie = x => orElse (() => die (x))

// --- @todo alleycat-js
export const any = (...fs) => {
  const acc = []
  for (const f of fs) {
    const ret = f (...acc)
    if (ret) return ret
    acc.push (ret)
  }
  return false
}

// @todo stick
export const propOfElseV = recurry (3) (
  def => o => k => o [k] | defaultToV (def),
)
export const propOfElseKey = recurry (3) (
  defaultKey => o => k => o [k] | defaultToV (o [defaultKey]),
)

// --- @todo useful? call-site is not that readable and ifNil works as well.
// export const propOfOrDie, export const lookUpInOrDie

// --- @todo lookUp, lookUpIn

// @todo alleycat-js
export const inputChecked = ['target', 'checked'] | path

export const whenNotEmptyListV = isNotEmptyList | whenPredicateV

export const flipFlop = (initial=0) => {
  let x = initial | Boolean
  return () => (x = x | not) | Number
}

/* Saga effects to put an action and wait for a dispatch */

export function *putTake (actionCreator, resolvedConstant) {
  yield actionCreator | sagaPut
  yield resolvedConstant | sagaTake
}

export function *putTakeN (args) {
  yield putTake (...args)
}

export const spread = passToN

// @todo stick
export const containsPred = recurry (2) (
  (p) => (xs) => ok (findIndex (p, xs)),
)

export const findIndex = recurry (2) (
  (p) => (xs) => {
    let i = -1
    for (const j of xs) {
      i += 1
      if (p (j)) return i
    }
    return void 8
  }
)

export const toSha256 = (msg) => recover (
  (err) => die ('Unable to make sha-256:' + err),
  letsP (
    () => new TextEncoder ('utf-8').encode (msg),
    (ary) => crypto.subtle.digest ('SHA-256', ary),
    (_, hb) => hb,
  ),
)
