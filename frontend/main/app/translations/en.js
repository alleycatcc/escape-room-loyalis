defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  isObject, assocPathM,
  tap, join,
  id,
  appendToM,
  assocM,
  lets,
  remapTuples,
} from 'stick-js/es'

import mergeAll from 'ramda/es/mergeAll'

import { transformIntl, } from 'alleycat-js/es/general'

// --- for <component>/messages.js
export const messages = {
  /*
  'app.containers.GenerateTab': {
    numWords: `{num} {num, plural,
      one {word}
      other {words}}
    `,
  },
  */
}

// --- for i18n.js
export default messages
  | remapTuples ((id, msgs) => transformIntl (id) (msgs))
  | mergeAll
