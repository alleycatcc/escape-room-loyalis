defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
  useMeasure,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  component,
} from '../../common'

import { Logo, GameHeader, } from '../shared'
import { Video, } from '../../components/Media'
import { Button, } from '../../containers/shared'

import Score from '../../containers/Score'
import Timer from '../../containers/Timer'

import config from '../../config'

import {} from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const WrapperS = styled.div`
  height: calc(100% - 100px);
  width: 100%;
`

const ButtonWrapperS = styled.div`
  display: ${prop ('show') >> ifFalseV ('none', 'block')};
  height: 100px;
  text-align: center;
`

const VideoWrapperS = styled.div`
  height: calc(100% - 300px);
`

const ScreenGameVideo = ({
  src,
  buttonTextNext,
  advance,
}) => {
  const [measure, callbackRef] = useMeasure ()
  const [buttonVisible, setButtonVisible] = useState (false)
  const showButton = useCallback (() => true | setButtonVisible)

  return <WrapperS>
    <GameHeader
      showTimer={false}
    />
    <VideoWrapperS ref={callbackRef}>
      {measure | whenOk (({ width, height, }) =>
        <Video
          src={src}
          width={width*0.95}
          height={height*0.95}
          onEnd={advance}
        />
      )}
    </VideoWrapperS>
    <ButtonWrapperS show={buttonVisible}>
      <Button
        onClick={advance}
        size='medium'
      >
        {buttonTextNext}
      </Button>
    </ButtonWrapperS>
  </WrapperS>
}

export default ScreenGameVideo
  | memoIgnore ([])
