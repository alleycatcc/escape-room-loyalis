defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, inputValue, keyPressListen, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, trim, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, ifEmptyString, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
  useMeasure,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  component,
} from '../../common'

import { Input, GameHeader, } from '../shared'
import { Text, Button, } from '../../containers/shared'

import config from '../../config'

import {} from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const colorSecondary = 'colors.secondary' | configGet

const WrapperS = styled.div`
  height: calc(100% - 100px);
  width: 100%;
`

const InputWrapperS = styled.div`
  height: 80px;
  width: 40%;
  margin: auto;
`

const ButtonWrapperS = styled.div`
  text-align: center;
`

const ScreenGameTeamname = component ('ScreenGameTeamname') (
  (props) => ({
    advance,
    setTeamName,
    buttonTextNext,
  }) => {
    const [teamNameOk, setTeamNameOk] = useState ()
    const onChange = useCallback ((event) =>
      event | inputValue | trim | ifEmptyString (
        () => false | setTeamNameOk,
        (name) => {
          true | setTeamNameOk
          name | setTeamName
        },
      )
    )

    const onKeyPress = keyPressListen (
      advance,
      'Enter',
    )

    return <WrapperS>
      <GameHeader
        showTimer={false}
      />
      <Text
        items={['Voer je team naam in']}
      />
      <InputWrapperS>
        <Input
          style={{fontSize: '30px'}}
          height='100%'
          onChange={onChange}
          // placeholder={'Teamnaam'}
          onKeyPress={onKeyPress}
        />
      </InputWrapperS>
      <ButtonWrapperS>
        <Button
          disabled={teamNameOk | not}
          onClick={advance}
          size='medium'
        >
          {buttonTextNext}
        </Button>
      </ButtonWrapperS>
    </WrapperS>
  },
)

export default ScreenGameTeamname
  | memoIgnore ([])
