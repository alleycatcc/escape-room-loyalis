defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  // andNot, and,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
} from '../../common'

import { GameHeader, Logo, } from '../shared'
import { Text, List, Button, } from '../../containers/shared'

import config from '../../config'

// import { } from '../../types'

import { debug, } from '../../debug'

// --- @todo alleycat-js
export const any = (...fs) => {
  const acc = []
  for (const f of fs) {
    const ret = f (...acc)
    if (ret) return ret
    acc.push (ret)
  }
  return false
}

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const GameIntroScreenS = styled.div`
  height: 100%;
  width: 100%;
  color: white;
`

const TitleS = styled.div`
  font-size: 40px;
  font-weight: bold;
  text-align: center;
`

const ButtonWrapperS = styled.div`
  text-align: center;
`

const TextWrapperS = styled.div`
  margin: 25%;
  margin-top: 7%;
  margin-bottom: 2%;
`

const ScreenGameIntro = deconstructProps (
  (props) => ({
    text,
    textItemList,
    buttonTextNext,
    advance,
  }) => {
    why ('ScreenGameIntro', props, {
    })

    const [items, TextComponent] = cond (
      [() => text, () => [text, Text]],
      [() => textItemList, () => [textItemList, List]],
      [T, () => die ('intro: missing both text & text-items')],
    )

    return <GameIntroScreenS>
      <GameHeader
        showTimer={false}/>
      <TextWrapperS>
        <TextComponent items={items}/>
      </TextWrapperS>
      <ButtonWrapperS>
        <Button
          onClick={advance}
          size='medium'
        >
          {buttonTextNext}
        </Button>
      </ButtonWrapperS>
    </GameIntroScreenS>
  },
)

export default ScreenGameIntro
  | memoIgnore ([])
