defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  divideBy,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, withDisplayName, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import WebcamMod from 'react-webcam'

import { Nothing, Just, cata, fold, foldJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, keyPressListen, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import VimeoPlayer from '@vimeo/player'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
} from '../../common'
import Timer from '../../containers/Timer'
import Score from '../../containers/Score'

import { Logo, Input, } from '../shared'
import { Text, List, Button, } from '../../containers/shared'

import config from '../../config'

import {} from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const why = debugRender ? whyRender : noop

const listen = 'addEventListener' | side2
const whenGreaterThan = gt >> whenPredicate

const ButtonWrapperS = styled.div`
  margin-top: -20px;
  text-align: center;
  width: 100%;
`
export const Webcam = ({ buttonTextTakePicture, width, height, facingMode='user', onCapture=noop, submitPictureSound }) => lets (
  () => ({
    width,
    height,
    facingMode,
  }),
  (videoConstraints) => {
    const webcamRef = useRef (null)
    const startCountdown = useCallback (() => {
      // triggers useEffect
      setTimeLeft (3)
      setTimeout(() => submitPictureSound (), 2300)
    })

    const [timeLeft, setTimeLeft] = useState (void 8)

    useEffect (() => {
      if (timeLeft | nil ) return
      if (timeLeft <= 0 ) {
        // --- base64 data with 'data:image/<mimetype>;base64' prefix
        webcamRef.current.getScreenshot () | onCapture
        setTimeLeft ( void 8 )
        return
      }
      setTimeout( () => setTimeLeft (timeLeft - 1 ), 1000)
    },
    [timeLeft]
    )

    const CountdownS = styled.div`
      position: absolute;
      top: 50%;
      left: 50%;
      font-size: 400px;
      font-weight: bold;
      transform: translateX(-50%) translateY(-50%);
      z-index: 100;
      animation-name: slowly-appear;
      animation-duration: 1s;
      animation-iteration-count: 1;
      @keyframes slowly-appear {
        0% {
          opacity: 1;
          font-size: 100px;
          }
        100% {
          opacity: 0;
          font-size: 1300px;
          }
      };
    `

    return <Fragment>
      <ButtonWrapperS>
        <WebcamMod
          audio={false}
          height={height}
          width={width}
          screenshotQuality={0.9}
          imageSmoothing={true}
          ref={webcamRef}
          screenshotFormat='image/jpeg'
          videoConstraints={videoConstraints}
        />
      </ButtonWrapperS>
      {timeLeft | whenOk (() => <CountdownS>
          {timeLeft}
        </CountdownS>)}
      <ButtonWrapperS>
        <Button
          onClick={startCountdown}
          size='medium'
        >
          {buttonTextTakePicture}
        </Button>
      </ButtonWrapperS>
    </Fragment>
  },
)

export const Sound = ({ src, type, duration=Nothing, }) => {
  const audioRef = useRef ()

  useEffect (() => {
    audioRef.current | whenOk ((ref) => ref
      | listen ('play', noop)
      | listen ('pause', noop)
      | listen ('complete', noop)
      | listen ('timeupdate', () => duration | foldJust (
        (ms) => ref.currentTime | multiply (1000) | whenGreaterThan (ms) (
          () => ref.pause (),
        ),
      )),
    )
  }, [])

  return <audio autoPlay ref={audioRef}>
    <source src={src} type={type}/>
    Your browser doesn&apos;t support the &lt;audio&gt; tag.
  </audio>
}

const videoSrcAutoplay = (autoplay) => (src) => autoplay
  | Number
  | sprintf1 ('?autoplay=%d')
  | concatTo (src)

const VideoS = styled.iframe`
  border: 0;
`

const VideoWrapperS = styled.div`
  position: relative;

  margin: auto;
  width: ${prop ('width') >> String >> concat ('px')};
  height: ${prop ('height') >> String >> concat ('px')};
`

// --- put an overlay over the video to catch keyboard events (like 'v', 'd', etc., which are Vimeo
// shortcuts).
// --- the div won't be visible but it does catch the events (tested on Firefox, Chromium).

const VideoOverlayS = styled.div`
  ${prop ('disableKeys') >> ifTrueV (
    'display: block',
    'display: none',
  )};

  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  z-index: 100;
`

export const Video = ({ src, height, width, autoplay=true, onEnd=noop, }) => {
  const iframeRef = useRef ()
  const listen = side2 ('on')

  useEffect (() => {
    const { current: ref, } = iframeRef
    new VimeoPlayer (ref)
      | listen ('play', noop)
      | listen ('ended', onEnd)
  }, [])

  return <VideoWrapperS
    width={width}
    height={height}
  >
    <VideoS
      ref={iframeRef}
      src={src | videoSrcAutoplay (autoplay)}
      width={width}
      height={height}
      frameborder='0'
      allow='autoplay'
      autoplay
    />
    <VideoOverlayS disableKeys={true}/>
  </VideoWrapperS>
}
