defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, whenOk, dot,
  drop, assoc,
  tap, eq,
  condS, guard, otherwise, guardV,
  id,
  always,
  prop, path,
  merge,
} from 'stick-js/es'

// import { } from '../../containers/App/constants'

import { logWith, } from 'alleycat-js/es/general'
import { reducer, traceReducer, } from 'alleycat-js/es/redux'

const TRACE = false

const initialState = {
}

const reducerTable = {
  // [SET_SIZE]: ({ data: size }) => merge ({
    // fontSize: size,
  // }),
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)
  | traceReducer ('ui', TRACE, action)
