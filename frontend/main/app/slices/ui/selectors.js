defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight, add, whenOk,
  tap,
  prop,
  ifOk, always,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { ifEquals, } from 'alleycat-js/es/predicate'

const selectSlice = (store, props) => store | prop ('ui')
