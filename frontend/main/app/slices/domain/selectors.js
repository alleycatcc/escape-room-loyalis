// ------ sending a 'slice' parameter to a makeSelector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gt, ok, ne, eq,
  prop,
  divideBy, minus,
  whenFalsy, lets,
  defaultToV,
  head,
  take,
  tap,
  ifPredicate,
  reduce, sprintfN,
  appendToM, mergeToM, id, T, F,
  recurry,
  bindProp,
  timesF, always,
  die,
  last, not, whenNil,
  multiply, noop,
  xMatch,
  list,
  zipAll,
  find,
  lt,
  deconstruct,
  condS, guardV, otherwise,
  lte,
  defaultTo,
  sprintf1,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

// import memoize1 from 'memoize-one'

import { fold, toJust, cata, isJust, Just, foldJust, liftA2N, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, ierror, reduceX, min, max, roundUp, slice, zipWithN, length, pluckN, } from 'alleycat-js/es/general'
import { ifEmptyList, anyV, isNotEmptyList, } from 'alleycat-js/es/predicate'

import { orDie, containsPred, } from '../../common'
import config from '../../config'
import {
  toScn, gameInfoPassword,
  CountdownSeverityNormal, CountdownSeverityLight, CountdownSeverityMedium, CountdownSeveritySevere,
  isScnGame, isScnGameBonus, isScnTextBetweenGames, isScnTextBeforeGame, isScnGameTeamname,
  isScnTakePhoto,
} from '../../types'

const { configGet, configGets, configGetOrDie, configGetsOrDie, } = config | configure.init

const scoreAmounts= 'scoreAmounts' | configGet
const timeRewards = 'timeRewards' | configGet

const calculateTimeReward = (timeRewards, time) => {
  const timeMinutesLeft = time / 60000
  const timeMinutesPassed = Math.floor (45 - timeMinutesLeft)
  const rewardMinute = (minutes) => {
    if (minutes < 0) {
      ierror ('Unexpected: calculateTimeReward: minutes < 0, returning 0')
      return 0
    }
    if (timeRewards [minutes] | ok)
      return timeRewards [minutes]
    else {
      return rewardMinute (minutes - 1)
    }
  }
  return rewardMinute (timeMinutesPassed)
}

const calculate = (what) => (scoreAmounts, gameIdx, time) => {
  const timeMinutesLeft = time / 60000
  const timeMinutesPassed = Math.floor (45 - timeMinutesLeft)
  const scoreAmountMinute = (minutes) => {
    if (minutes < 0) {
      ierror ('Unexpected: calculate: minutes < 0, returning safe value')
      return []
    }
    if (scoreAmounts[minutes])
      return scoreAmounts[minutes]
    else
      return scoreAmountMinute (minutes - 1)
  }

  const data = scoreAmountMinute (timeMinutesPassed) [gameIdx] | defaultTo (() => {
    ierror (gameIdx | sprintf1 ('calculate: no data for game idx %s, returning safe values'))
    return { hintPrice: 250, reward: 800, }
  })

  if (what === 'hints')
    return data.hintPrice
  if (what === 'reward')
    return data.reward
}

const calculateHintCost = calculate ('hints')
const calculateReward = calculate ('reward')

const selectSlice = (store, props) => store | prop ('domain')

export const makeSelectError = _ => createSelector (
  selectSlice,
  prop ('error'),
)

export const makeSelectCurScreenIdx = _ => createSelector (
  selectSlice,
  prop ('curScreenIdx'),
)

export const makeSelectTimerRunning = _ => createSelector (
  selectSlice,
  prop ('timerRunning'),
)

// export const makeSelectTimerChannel = _ => createSelector (
  // selectSlice,
  // prop ('timerChannel'),
// )

export const makeSelectCurTime = _ => createSelector (
  selectSlice,
  prop ('curTime'),
)

export const makeSelectCurScore = _ => createSelector (
  selectSlice,
  prop ('curScore'),
)

export const makeSelectFinalScore = _ => createSelector (
  selectSlice,
  prop ('finalScore'),
)

export const makeSelectLastPasswordEntered = _ => createSelector (
  selectSlice,
  prop ('lastPasswordEntered'),
)

const makeSelectGameInfo = _ => createSelector (
  selectSlice,
  prop ('gameInfo'),
)

// --- meant to be called from saga.
// @throws on Nothing.
export const makeSelectCorrectPassword = _ => createSelector (
  makeSelectGameInfo (),
  gameInfoPassword >> orDie ('makeSelectCorrectPassword: nil'),
)

export const makeSelectEscapeConfig = _ => createSelector (
  selectSlice,
  prop ('escapeConfig'),
)

export const makeSelectHasEscapeConfig = _ => createSelector (
  makeSelectEscapeConfig (),
  isJust,
)

// --- must be sure the escape config has been loaded (using e.g. `makeSelectHasEscapeConfig`)
// before using this or any of the selectors which derive from this.
// --- @throws
// --- @todo memoise

const makeSelectEscapeConfigGet = _ => createSelector (
  makeSelectEscapeConfig (),
  (configMb) => configMb | fold (
    (config) => lets (
      () => config | configure.init | prop ('configGetOrDie'),
      (get) => (val) => val | get,
    ),
    () => die ('escape config not loaded yet'),
  ),
)

export const makeSelectTimerStart = _ => createSelector (
  makeSelectEscapeConfigGet (),
  (get) => 'timer.start' | get,
)

export const makeSelectScoreStart = _ => createSelector (
  makeSelectEscapeConfigGet (),
  (get) => 'score.start' | get,
)

export const makeSelectScreens = _ => createSelector (
  makeSelectEscapeConfigGet (),
  (get) => 'screens' | get,
)

export const makeSelectHintText = _ => createSelector (
  makeSelectEscapeConfigGet (),
  (get) => 'hint-text' | get,
)

export const makeSelectHintCost = _ => createSelector (
  () => scoreAmounts,
  makeSelectGameScreenIdx (),
  makeSelectCurTime (),
  (amounts, idxMb, time) => idxMb | foldJust (
    (idx) => calculateHintCost (amounts, idx, time),
  ),
)

export const makeSelectGameReward = _ => createSelector (
  () => scoreAmounts,
  makeSelectGameScreenIdx (),
  makeSelectCurTime (),
  (amounts, idxMb, time) => idxMb | foldJust (
    (idx) => calculateReward (amounts, idx, time),
  ),
)

export const makeSelectNumScreens = _ => createSelector (
  makeSelectScreens (),
  length,
)

// :: -> screen-spec
export const makeSelectCurScreen = _ => createSelector (
  makeSelectScreens (),
  makeSelectCurScreenIdx (),
  (sx, idx) => sx [idx],
)

export const makeSelectGameScreenIdx = _ => createSelector (
  selectSlice,
  prop ('gameScreenIdx'),
)

export const makeSelectIsGameScreen = _ => createSelector (
  makeSelectCurScreenType (),
  isScnGame,
)

export const makeSelectIsTextBeforeGameScreen = _ => createSelector (
  makeSelectCurScreenType (),
  isScnTextBeforeGame,
)

export const makeSelectIsScreenGameTeamname = _ => createSelector (
  makeSelectCurScreenType (),
  isScnGameTeamname,
)

export const makeSelectIsScreenTakePhoto = _ => createSelector (
  makeSelectCurScreenType (),
  isScnTakePhoto,
)

export const makeSelectIsTextBetweenGamesScreen = _ => createSelector (
  makeSelectCurScreenType (),
  isScnTextBetweenGames,
)

export const makeSelectIsGameBonusScreen = _ => createSelector (
  makeSelectCurScreenType (),
  isScnGameBonus,
)

export const makeSelectIsFirstGameScreen = _ => createSelector (
  makeSelectGameScreenIdx (),
  fold (0 | eq, false),
)

export const makeSelectCurScreenType = _ => createSelector (
  makeSelectCurScreen (),
  prop ('type') >> toScn,
)

const makeSelectHintCategories = _ => createSelector (
  selectSlice,
  prop ('hintCategories'),
)

const makeSelectHints = _ => createSelector (
  selectSlice,
  prop ('hints'),
)

/* Legacy version of this function.
 * Useful to keep as an example of how to use `liftA2N`.
 *
 * // :: Maybe [{ name, canBuy, }]
 * export const makeSelectHintCategoryData = _ => createSelector (
 *   makeSelectHintCategories (),
 *   makeSelectHints (),
 *
 *   (catsMb, hintsMb) => [catsMb, hintsMb] | liftA2N (
 *     // --- must be curried.
 *     (cats) => (hints) => [cats, hints] | zipWithN (
 *       (cat, hintsForCat) => ...,
 *     ),
 *   ),
 * )
 */

export const makeSelectHintCategoryData = _ => createSelector (
  makeSelectHintCategories (),
  makeSelectHints (),

  (cats, hints) => [cats, hints] | zipWithN (
    ({ name, }, hintsForCat) => ({
      name,
      canBuy: hintsForCat | isNotEmptyList,
    }),
  ),
)

export const makeSelectCanBuyAHint = _ => createSelector (
  makeSelectHintCategoryData (),
  containsPred (prop ('canBuy')),
)

export const makeSelectBonusReceived = _ => createSelector (
  () => timeRewards,
  makeSelectCurTime (),
  (rewards, time) => Just (calculateTimeReward (rewards, time)),
)

export const makeSelectLastScoreGained = _ => createSelector (
  selectSlice,
  prop ('lastScoreGained')
)

export const makeSelectTeamName = _ => createSelector (
  selectSlice,
  prop ('teamName'),
)

export const makeSelectHintsBought = _ => createSelector (
  selectSlice,
  prop ('hintsBought'),
)

export const makeSelectHintsBoughtForUpload = _ => createSelector (
  makeSelectHintsBought (),
  map (pluckN (['text', 'category'])),
)


export const makeSelectCountdownSeverity = _ => createSelector (
  makeSelectCurTime (),
  (ms) => ms | condS ([
    120000 | lt | guardV (CountdownSeveritySevere),
    240000 | lt | guardV (CountdownSeverityMedium),
    300000 | lt | guardV (CountdownSeverityLight),
    otherwise   | guardV (CountdownSeverityNormal),
  ])
)

export const makeSelectTimeUp = _ => createSelector (
  makeSelectCurTime (),
  0 | lte,
)

export const makeSelectAnimateText = _ => createSelector (
  makeSelectIsGameScreen (),
  makeSelectIsTextBeforeGameScreen (),
  (a, b) => a || b,
)

export const makeSelectMainSoundKey = _ => createSelector (
  selectSlice,
  prop ('mainSoundKey'),
)

export const makeSelectShowAdminTools = _ => createSelector (
  selectSlice,
  prop ('showAdminTools'),
)

export const makeSelectShowAdminToolsAllowed = _ => createSelector (
  makeSelectShowAdminTools (),
  makeSelectLoggedIn (),
  (show, loggedIn) => show && loggedIn,
)

export const makeSelectLoggedIn = _ => createSelector (
  selectSlice,
  prop ('loggedIn'),
)

export const makeSelectUid = _ => createSelector (
  selectSlice,
  prop ('uid'),
)

export const makeSelectPhotoTeam = _ => createSelector (
  selectSlice,
  prop ('photoTeam'),
)
