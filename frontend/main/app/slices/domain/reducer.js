defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  assoc,
  id, always, tap, noop, ifTrue,
  lets,
  prop,
  arg1,
  reduce,
  map,
  take, head,
  mergeTo,
  concat,
  condS, eq, guard, otherwise,
  precat, die, defaultTo, sprintf1,
  dot2,
  timesV,
  merge,
  whenPredicate,
  gt,
  divideBy,
  minus,
  deconstruct2,
  plus, update,
  ifOk,
  concatTo, precatTo,
  T, F,
  recurry,
  dot1,
  deconstruct,
  append, last,
  tail, list,
  not,
} from 'stick-js/es'

import { select, } from 'redux-saga/effects'

import { foldJust, Just, Nothing, cata, fold, toJust, toMaybe, flatMap, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, defaultToV, composeManyRight, slice, roundUp, toString, length, notBelow, } from 'alleycat-js/es/general'
import { reducer, traceReducer, } from 'alleycat-js/es/redux'

import {
  ADMIN_TOOLS_DISPLAY,
  SCREEN_ADVANCE,
  SCREEN_ADVANCE_DO,
  SCREEN_ADVANCED,
  GAME_INFO_SET,
  GAME_SCREEN_ENTERED,
  HINT_BUY,
  HINT_TAKE,
  PASSWORD_ENTERED_CORRECT,
  PASSWORD_ENTERED_INCORRECT,
  LAST_PASSWORD_ENTERED_CLEAR,
  LOGGED_IN,
  ESCAPE_CONFIG_FETCHED,
  SOUND_PLAY,
  TEAM_NAME_SET,
  TIMER_STARTED,
  TIMER_SET,
  TIMER_STOP,
  TIMER_START,
  TIME_ELAPSED,
  SCORE_FINAL_SET,
  SCORE_RESET,
  SCORE_UPDATE,
  SLICE_DOMAIN_REPLACE,
  SUBMIT_PICTURE,
  UID_FETCHED,
  UPDATE_LAST_SCORE_GAINED,
} from '../../containers/App/constants'

import { assocPathList, untag, init, arrow, assocList, envIsTstLike, } from '../../common'
import config from '../../config'
import {
  PasswordNone, PasswordCorrect, PasswordIncorrect,
  Hint,
  picklePassword, unpicklePassword,
  pickleHint, unpickleHint,
} from '../../types'

import {
  pickleMaybe, unpickleMaybe,
} from '../../pickle/pickleTypes'

const TRACE = false

const increment = 1 | plus
const headTail = arrow (head, tail, list)

const { configGet, configGets, configGetOrDie, } = config | configure.init

// :: Number [0 or 1] -> Number [0 or 1]
const flip = Boolean >> not >> Number

const pickleHints         = map (map (pickleHint))
const unpickleHints       = map (map (unpickleHint))
const pickleHintsBought   = map (pickleHint)
const unpickleHintsBought = map (unpickleHint)

export const pickleSpec = {
  escapeConfig: [pickleMaybe, unpickleMaybe],
  lastPasswordEntered: [picklePassword, unpicklePassword],
  gameScreenIdx: [pickleMaybe, unpickleMaybe],
  hints: [pickleHints, unpickleHints],
  hintsBought: [pickleHintsBought, unpickleHintsBought],
}

const initialState = {
  // --- `error=true` means the reducer is totally corrupted and the app should halt.
  error: false,

  escapeConfig: Nothing,
  curScreenIdx: 0,
  curTime: void 8,
  curScore: void 8,
  // --- `finalScore` is set as soon as it's known -- this is useful because `curScore` might still be
  // animating and we don't want to wait for it to stop.
  finalScore: void 8,
  // :: Maybe idx
  // --- Just 0 if first game screen, Nothing if not a game screen.
  gameScreenIdx: Nothing,

  // :: GameInfo
  gameInfo: void 8,

  // :: [{ name, }]
  hintCategories: [],
  // :: [[Hint]]
  hints: [],
  // :: [Hint]
  hintsBought: [],
  lastPasswordEntered: PasswordNone,
  loggedIn: true,
  photoTeam: void 8,
  showAdminTools: envIsTstLike,
  teamName: void 8,
  timerRunning: false,
  uid: void 8,
}

const reducerTable = {
  [ADMIN_TOOLS_DISPLAY]: ({ data: showAdminTools, }) => merge ({
    showAdminTools,
  }),
  [SCREEN_ADVANCE_DO]: () => update ('curScreenIdx', increment),
  [SCREEN_ADVANCED]: () => merge ({
    hintCategories: [],
    hints: [],
    hintsBought: [],
  }),
  [PASSWORD_ENTERED_CORRECT]: () => merge ({
    lastPasswordEntered: PasswordCorrect
  }),
  [PASSWORD_ENTERED_INCORRECT]: () => merge ({
    lastPasswordEntered: PasswordIncorrect
  }),
  [LAST_PASSWORD_ENTERED_CLEAR]: () => merge ({
    lastPasswordEntered: PasswordNone
  }),
  [SCORE_FINAL_SET]: ({ data: finalScore, }) => merge ({
    finalScore,
  }),
  [SCORE_RESET]: ({ data: resetScore, }) => merge ({
    curScore: resetScore,
    finalScore: void 8,
  }),
  [SCORE_UPDATE]: ({ data: points, }) => update ('curScore', points | plus),
  [SLICE_DOMAIN_REPLACE]: prop ('data') >> always,
  [SUBMIT_PICTURE]: ({ data: photoTeam, }) => merge ({
    photoTeam,
  }),
  [TEAM_NAME_SET]: ({ data: teamName, }) => merge ({
    teamName,
  }),
  [TIMER_SET]: ({ data: curTime, }) => merge ({
    curTime,
  }),
  [TIMER_START]: () => assoc (
    'timerRunning', true,
  ),
  [TIMER_STOP]: () => assoc (
    'timerRunning', false,
  ),
  [TIME_ELAPSED]: ({ data: ms, }) => update (
    'curTime',
    minus (ms) >> notBelow (0),
  ),
  [UPDATE_LAST_SCORE_GAINED]: ({ data: amount, }) => merge ({
    lastScoreGained: amount,
  }),
  [ESCAPE_CONFIG_FETCHED]: ({ data: config, }) => merge ({
    escapeConfig: config | Just,
  }),
  [GAME_SCREEN_ENTERED]: () => [
    update (
      'gameScreenIdx',
      fold (increment >> Just, 0 | Just),
    ),
    merge ({
      hintsBought: [],
    }),
  ],
  [GAME_INFO_SET]: ({ data: gameInfo, }) => gameInfo | untag (
    (_, hints) => merge ({
      gameInfo,
      // --- @todo move to selector?
      hintCategories: hints | map (
        ({ name, subhints=[], }) => ({
          name,
        }),
      ),
      // --- `hints` = what we store in the reducer.
      hints:
        // --- `hints` = the hints as they came in through the config.
        hints | map (
          ({ name, subhints=[], }) => subhints | map (
          // prop ('subhints') >> map (
            deconstruct (
              ({ text, image=null, }) => Hint (name, text, image | toMaybe),
            ),
          ),
        ),
    }),
  ),
  [HINT_TAKE]: ({ data: categoryIdx, }) => deconstruct2 (
    ({ hintsBought, hints, hintStatus, }) => merge (lets (
      () => hints | prop (categoryIdx) | headTail,
      ([hint, remaining]) => ({
        hintsBought: hintsBought | append (hint),
        // --- @todo
        // assocList doesn't work because it wants list of lists (and we have ADTs in the list).
        // assoc doesn't work because it converts lists to objects.
        hints: hints | deconstruct (
          (xs) => [...xs] | tap (
            ys => ys [categoryIdx] = remaining,
          ),
        ),
      }),
    )),
  ),
  [LOGGED_IN]: () => merge ({
    loggedIn: true,
  }),
  [UID_FETCHED]: ({ data: uid, }) => merge ({
    uid,
  }),
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)
  | traceReducer ('domain', TRACE, action)
