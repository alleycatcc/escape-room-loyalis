defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  assoc, eq, id,
  condS, guard, otherwise, guardV,
  prop, dot1, tap, drop,
  lets,
  prepend,
  path, ifTrue,
  take, merge, not, split,
  deconstruct, deconstruct2,
} from 'stick-js/es'

import { logWith, composeManyRight, pop, } from 'alleycat-js/es/general'
import { ifPredicateResult, } from 'alleycat-js/es/predicate'
import { reducer, traceReducer, } from 'alleycat-js/es/redux'

import { browseLinkForRouter, } from '../../common'

// import { } from '../../containers/App/constants'

const TRACE = false

const initialState = {
  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,
}

const reducerTable = {
  // [HISTORY_BROWSED]: ({ data: { url, wasBack, }}) => null,
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)
  | traceReducer ('app', TRACE, action)
