defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  defaultTo,
} from 'stick-js'

export const propOfElse = recurry (3) (
  f => o => k => o [k] | defaultTo (f),
)

export const propOfOrDie = recurry (2) (
  o => k => propOfElse (
    () => die ('Unable to get prop ' + k + ' in object'),
    o, k,
  ),
)

export const mapGetElse = recurry (3) (
  f => o => k => o.get (k) | defaultTo (f),
)

export const mapGetOrDie = recurry (2) (
  o => k => mapGetElse (
    () => die ('Unable to get key ' + k + ' in map'),
    o, k,
  ),
)

