defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
} from 'stick-js'

import { cata, } from 'alleycat-js/es/bilby'

export const pickle = recurry (2) (
  (spec) => (o) => {
    const pickled = {}
    for (const k in o) {
      const h = spec [k]
      const [pickler, unpickler] = h ? h : [id, id]
      pickled [k] = pickler (o [k])
    }
    return pickled
  },
)

export const unpickle = recurry (2) (
  (spec) => (o) => {
    const ret = {}
    for (const k in o) {
      const h = spec [k]
      const [pickler, unpickler] = h ? h : [id, id]
      ret [k] = unpickler (o [k])
    }
    return ret
  },
)

export const finishPickle = JSON.stringify
export const startUnpickle = JSON.parse
