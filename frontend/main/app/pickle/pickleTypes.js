defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  invoke,
  applyToN,
  list,
  passToN,
  isArray,
} from 'stick-js'

import { Just, Nothing, fold, cata, } from 'alleycat-js/es/bilby'
import { logWith, } from 'alleycat-js/es/general'
import { mapGetOrDie, } from './stick-extra'

const ifArray = isArray | ifPredicate

export const pickleTaggedSum = (o) => {
  const p = {}
  for (const constructor in o) {
    const v = o [constructor]
    const [constructorIdx, f] = v | ifArray (
      id,
      (idx) => [idx, list],
    )
    p [constructor] = (...fields) => [constructorIdx, f (...fields)]
  }
  return cata (p)
}

export const unpickleTaggedSum = (xs) => invoke (() => {
  const o = new Map (xs)
  return ([constructorIdx, vals]) => constructorIdx | mapGetOrDie (o) | applyToN (vals)
})

export const pickleTagged = (o) => {
  if (!o.untag) die (`Missing untag function, can't pickle: ` + o.toString ())
  return o.untag (list)
}

export const unpickleTagged = passToN

// --- maybe and either don't follow the pattern of cata
export const pickleMaybe = fold (
  (...args) => [0, args],
  [1, []],
)

export const unpickleMaybe = unpickleTaggedSum ([
  [0, Just],
  [1, Nothing | always],
])
