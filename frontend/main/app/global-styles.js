defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight, sprintf1,
  ifTrue,
} from 'stick-js/es'

import { createGlobalStyle, } from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhone, mediaTablet, mediaDesktop, mediaDesktopBig, } from './common'

import config from './config'

const { configGet, configGets, configGetsOrDie, } = config | configure.init

const fontPrimaryUrl = 'font.primary.url' | configGet
const fontPrimaryFamily = 'font.primary.family' | configGet
const fontSecondaryUrl = 'font.secondary.url' | configGet
const fontMonospaceUrl = 'font.monospace.url' | configGet
const colorBackground = 'colors.alternate' | configGet
const colorPrimary = 'colors.primary' | configGet
const backgroundImage = 'images.background' | configGet
const colorSecondary = 'colors.secondary' | configGet

const fontPrimaryCss = fontPrimaryUrl | sprintf1 ('@import url(%s);')
const fontSecondaryCss = fontSecondaryUrl | sprintf1 ('@import url(%s);')
const fontMonospaceCss = fontMonospaceUrl | sprintf1 ('@import url(%s);')

// --- remember to use normal syntax for injecting variables (normal strings, not functions).

export const GlobalStyle = createGlobalStyle`
  ${fontPrimaryCss}
  ${fontSecondaryCss}
  ${fontMonospaceCss}

  // --- note: on mobile, height = 100vh includes url bar height and soft buttons, while 100%
  // doesn't.

  html, body {
    height: 100%;
    width: 100%;
    position: fixed;
    overflow: hidden;
  }

  body {
    font-family: ${fontPrimaryFamily};
    color: ${colorPrimary};
    background: black;
    background-image: url(${backgroundImage});
    background-attachment: fixed;
    background-size: 100%;
    ${mediaQuery (
      mediaPhone ('font-size: 10px;'),
      mediaDesktop ('font-size: 23px;'),
      mediaDesktopBig ('font-size: 32px;'),
    )}
    &.x--drama {
      animation-name: body-shake;
      animation-duration: 6.0s;
      animation-iteration-count: infinite;
      // --- ease-in-out-back
      animation-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
    }
    &.x--drama-extra {
      animation-duration: 1.2s;
    }
    @keyframes body-shake {
      0%, 16% { left: 0px; top: 0px; }
      3%, 10% { left: 20px; top: 3px; }
      6%, 13% { left: -15px; }
    }
  }

  h1, h2, h3, h4, p {
    cursor: text;
  }

  #app {
    height: 100%;
    width: 100%;
  }

  .u-cursor-pointer {
    cursor: pointer;
  }

  .u--hide {
    display: none;
  }

  .ReactModal {
    &__Content {
      height: 0;
      width: 0;
      opacity: 0.5;
      transition: height 200ms;
      &--after-open {
        height: 90%;
        width: 60%;
        opacity: 1;
      }
    }
  }
`
