defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
  useMeasure,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  makeSelectTimeUp,
} from '../../slices/domain/selectors'

import { Logo, GameHeader, } from '../../components/shared'
import { Text, Button, } from '../../containers/shared'

import Timer from '../../containers/Timer'
import Score from '../../containers/Score'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  component,
} from '../../common'
import config from '../../config'
import { debug, } from '../../debug'
import {} from '../../types'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const WrapperS = styled.div`
  height: 100%;
  width: 100%;
  margin-top: 20px;
`

// --- @todo duplicated
const TimerWrapperS = styled.div`
  margin: auto;
  margin-top: 60px;
  width: 60%;
`

// --- @todo duplicated from ScreenGame
const ButtonWrapperS = styled.div`
  text-align: center;
  font-size: 80px;
  margin-top: 60px;
`

const ScreenOver = component ('ScreenOver') (
  (props) => ({
    advance,
    timeUp,
    timeRemainingButtonTextNext,
    timeRemainingText,
    timeUpButtonTextNext,
    timeUpText,
  }) => {
    return <WrapperS>
      <GameHeader
        showTimer={false}
      />
      {timeUp | ifTrue (
        () => null,
        () => <TimerWrapperS>
          <Timer fontSize='130px'/>
        </TimerWrapperS>,
      )}
      {timeUp | ifTrue (
        () => <Text big={true} items={timeUpText}/>,
        () => <Text big={true} items={timeRemainingText}/>,
      )}
      <ButtonWrapperS>
        <Button
          onClick={advance}
          size='medium'
        >
          {timeUp | ifTrueV (
            timeUpButtonTextNext,
            timeRemainingButtonTextNext,
          )}
        </Button>
      </ButtonWrapperS>
    </WrapperS>
  },
)

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  timeUp: makeSelectTimeUp (),
})

export default ScreenOver
  | connect       (mapStateToProps, mapDispatchToProps)
  | memoIgnore ([])
