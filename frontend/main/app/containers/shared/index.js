defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  prop, whenTrue, sprintf1,
  ifOk, always,
  tap, id, lets, mergeTo,
  whenOk, concatTo,
  sprintfN,
  arg0, merge,
  eq, condS, guardV,
  invoke, ifFalse,
  dot,
} from 'stick-js/es'

import React, { Fragment, useRef, useState, useEffect, useCallback, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, mapX, length, slice, } from 'alleycat-js/es/general'
import { ifTrueV, whenTrueV, } from 'alleycat-js/es/predicate'
import { ElemP, deconstructProps, withDisplayName} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  makeSelectAnimateText,
} from '../../slices/domain/selectors'

import {
  submitButtonSound,
} from '../App/actions'

import { spinner, } from '../../alleycat-components'

import Score from '../../containers/Score'
import TeamName from '../../containers/TeamName'
import Timer from '../../containers/Timer'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, component, propOfElseKey, } from '../../common'
import config from '../../config'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const toUpper = 'toUpperCase' | dot

const colorSecondary = 'colors.secondary' | configGet
const colorAlternate = 'colors.alternate' | configGet
const colorPrimary = 'colors.primary' | configGet
const fontPrimaryFamily = 'font.primary.family' | configGet
const fontSecondaryFamily = 'font.secondary.family' | configGet
const logo = 'images.logo' | configGet
const buttonBackground = 'images.button' | configGet
const buttonWideBackground = 'images.buttonWide' | configGet

const diamond = 'images.diamond' | configGet

const ImgDiamondS = styled.div`
  img {
    vertical-align: baseline;
  }
`

const ImgDiamond = () => <ImgDiamondS>
  <img src={diamond} width={20}/>
</ImgDiamondS>

const TextS = styled.div`
  &, div {
    text-align: ${prop('align')};
    display: ${prop('display')}
  }
`

const ListS = styled.div`
  text-align: left;
  margin-left: 5%;
  .x__item {
    .x__bullet {
      display: inline-block;
      padding-right: 10px;
      width: 40px;
      vertical-align: top;
    }
    .x__text {
      width: calc(100% - 40px);
      display: inline-block;
      margin-top: 0.3%;
      margin-bottom: 1%;
    }
  }
`

const TextWrapperS = styled.div`
  ${prop ('big') >> whenTrueV (`
    margin: 8%;
    font-size: 64px;
    font-family: ${fontSecondaryFamily};
  `)}
`

export const Text = invoke (() => {
  const Text = ({ big=false, items, animateTextProp=true, animateText=false, align='center', display='block' }) => lets (
    () => ({ 1: 60, 2: 170, 3: 280, }),
    (msTable) => items.length | propOfElseKey ('3', msTable),
    (_, ms) => <TextS align={align} display={display}>
      {items | mapX (
        (txt, idx) => {
          const text = (animateText && animateTextProp) | ifFalse (
            () => txt,
            () => invoke (() => {
              const i = useRef (0)
              const l = txt | length
              const [text, setText] = useState ('')
              useEffect (() => {
                const ii = i.current
                if (ii === l) return
                i.current += 1
                setTimeout (
                  () => setText (txt | slice (0, ii + 1)),
                  ms,
                )
              })
              return text
            }),
          )

          return <TextWrapperS
            key={idx}
            big={big}
            dangerouslySetInnerHTML={{ __html: lets (
              () => big ? toUpper : id,
              f => text | f,
            )}}
          />
        },
      )}
    </TextS>,
  )

  const mapDispatchToProps = (dispatch) => ({
  })

  const mapStateToProps = createStructuredSelector ({
    animateText: makeSelectAnimateText (),
  })

  return Text
    | connect       (mapStateToProps, mapDispatchToProps)
})

export const List = ({ items, }) => <ListS>
  {items | mapX (
    (txt, idx) => <div
      className='x__item'
      key={idx}
    >
      <div className='x__bullet'>
        <ImgDiamond/>
      </div>
      <div className='x__text'
        dangerouslySetInnerHTML={{ __html: txt, }}
      />
    </div>
  )}
</ListS>

// --- fakeDisabled means the onClick handler is called, but the button doesn't move when clicked.
// --- it's useful for e.g. letting the click event bubble to an outer comopnent.
const ButtonBaseS = styled.button`
  display: inline-block;
  ${prop ('fakeDisabled') >> ifTrueV (
    ``,
    `
    &:not(:disabled) {
      cursor: pointer;
      &:active {
        transition: all .03s;
        transform: translateY(1px) translateX(1px);
        opacity: 0.8;
      }
    }
    `,
  )}
  &:focus {
    outline: none;
  }
`

export const ButtonS = styled (ButtonBaseS)`
  ${prop ('size') >> condS ([
    'wide' | eq | guardV (`
      font-size: 26px;
      width: 290px;
      height: 58px;
    `),
    'small' | eq | guardV (`
      font-size: 28px;
      width: 200px;
      height: 54px;
    `),
    'medium' | eq | guardV (`
      font-size: 27px;
      width: 400px;
      height: 108px;
      padding: 25px;
      margin: 30px;
    `),
    'large' | eq | guardV (`
      font-size: 20px;
      width: 440px;
      height: 116px;
      padding: 36px;
      padding-left: 45px;
      padding-right: 45px;
    `),
  ])}
  ${prop ('wide') >> ifTrueV (
    `background: url(${buttonWideBackground});`,
    `background: url(${buttonBackground});`,
  )}
  background-size: 100%;
  color: ${colorAlternate};
  // --- needs to be here again.
  font-family: ${fontSecondaryFamily};
  border-radius: 2px;
  position: relative;
  opacity: 0.5;
  &:not(:disabled) {
    opacity: 1.0;
    }
  }
`
export const Button = invoke(() => {
  const Button = ( props ) => {
    const onClick = useCallback((...args) => {
      props.submitButtonSoundDispatch ()
      props.onClick (...args)
    })
    return <ButtonS type='submit' {...props} onClick={onClick}>
      <div>
      {props.children}
      </div>
      {/* <div className='x__overlay'/> */}
    </ButtonS>
  }

  const mapDispatchToProps = (dispatch) => ({
    submitButtonSoundDispatch: submitButtonSound >> dispatch,
  })

  const mapStateToProps = createStructuredSelector ({
  })

  return Button
    | connect       (mapStateToProps, mapDispatchToProps)
})
