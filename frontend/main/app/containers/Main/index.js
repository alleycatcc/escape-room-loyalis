defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  divideBy,
  recurry,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, withDisplayName, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { screenAdvance, timerStop, teamNameSet, } from '../App/actions'

import {
  makeSelectCurScreen, makeSelectCurTime,
  makeSelectLastPasswordEntered, makeSelectIsFirstGameScreen,
} from '../../slices/domain/selectors'

import ScreenText from '../../components/ScreenText'
import ScreenVideo from '../../components/ScreenVideo'
import ScreenTextBeforeGame from '../../containers/ScreenTextBeforeGame'
import ScreenTextBetweenGames from '../../containers/ScreenTextBetweenGames'
import ScreenGameVideo from '../../components/ScreenGameVideo'
import ScreenGameText from '../../components/ScreenGameText'
import ScreenGameTeamname from '../../components/ScreenGameTeamname'
import ScreenEnd from '../../components/ScreenEnd'

import ScreenGame from '../../containers/ScreenGame'
import ScreenGameBonus from '../../containers/ScreenGameBonus'
import ScreenGameOver from '../../containers/ScreenGameOver'
import ScreenLogin from '../../containers/ScreenLogin'
import ScreenTakePhoto from '../../containers/ScreenTakePhoto'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, component, } from '../../common'
import config from '../../config'

import { toScn, } from '../../types'

const { configGet, configGets, configGetOrDie, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const colorSecondary = 'colors.secondary' | configGet
const why = debugRender ? whyRender : noop

const gameImages = 'images.gameImages' | configGetOrDie

const getScreenComponent = ({
  spec,
  screenAdvanceDispatch,
  timerStopDispatch,
  teamNameSetDispatch,
}) => spec | deconstruct (
  ({ type, }) => type | toScn | cata ({
    ScnLogin: () => () => spec | deconstruct (({
    }) => <ScreenLogin
      advance={screenAdvanceDispatch}
    />),
    ScnText: () => () => spec | deconstruct (({
      text,
      'text-itemlist': textItemList,
      title=null,
      'button-text-next': buttonTextNext,
      'text-second': textSecond,
    }) => <ScreenText
      buttonTextNext={buttonTextNext}
      text={text}
      textItemList={textItemList}
      title={title}
      advance={screenAdvanceDispatch}
      textSecond={textSecond}
    />),
    ScnVideo: () => () => spec | deconstruct (({
      src,
      'button-text-next': buttonTextNext,
    }) => <ScreenVideo
      src={src}
      advance={screenAdvanceDispatch}
      buttonTextNext={buttonTextNext}
    />),
    ScnTextBeforeGame: () => () => spec | deconstruct (({
      text,
      'text-itemlist': textItemList,
      title=null,
      'button-text-next': buttonTextNext,
      'text-second': textSecond,
    }) => <ScreenTextBeforeGame
      buttonTextNext={buttonTextNext}
      text={text}
      textItemList={textItemList}
      title={title}
      advance={screenAdvanceDispatch}
      textSecond={textSecond}
    />),
    ScnGame: () => () => spec | deconstruct (({
      text,
      image: imageKey,
      'password-placeholder': passwordPlaceholder,
      'button-text-hint': buttonTextHint,
      'button-text-submit': buttonTextSubmit,
    }) => lets (
      () => gameImages [imageKey] || die ('No image for key: ' + imageKey),
      (image) => <ScreenGame
        buttonTextHint={buttonTextHint}
        buttonTextSubmit={buttonTextSubmit}
        text={text}
        image={image}
        advance={screenAdvanceDispatch}
        passwordPlaceholder={passwordPlaceholder}
      />,
    )),
    ScnTextBetweenGames: () => () => spec | deconstruct (({
      text,
      'text-itemlist': textItemList,
      title=null,
      'button-text-next': buttonTextNext,
      'text-second': textSecond,
    }) => <ScreenTextBetweenGames
      buttonTextNext={buttonTextNext}
      text={text}
      textItemList={textItemList}
      title={title}
      advance={screenAdvanceDispatch}
      textSecond={textSecond}
    />),
    ScnGameOver: () => () => spec | deconstruct (({
      data: {
        'time-remaining': {
          'text': timeRemainingText=[],
          'button-text-next': timeRemainingButtonTextNext,
        },
        'time-up': {
          'button-text-next': timeUpButtonTextNext,
          'text': timeUpText=[],
        },
      },
    }) => <ScreenGameOver
      advance={() => {
        timerStopDispatch ()
        screenAdvanceDispatch ()
      }}
      timeRemainingButtonTextNext={timeRemainingButtonTextNext}
      timeRemainingText={timeRemainingText}
      timeUpButtonTextNext={timeUpButtonTextNext}
      timeUpText={timeUpText}
    />),
    ScnGameBonus: () => () => spec | deconstruct (({
      data: {
        'time-remaining': {
          'text': timeRemainingText=[],
          'button-text-next': timeRemainingButtonTextNext,
        },
        'time-up': {
          'text': timeUpText=[],
          'button-text-next': timeUpButtonTextNext,
        },
      },
    }) => <ScreenGameBonus
      advance={screenAdvanceDispatch}
      timeRemainingButtonTextNext={timeRemainingButtonTextNext}
      timeRemainingText={timeRemainingText}
      timeUpButtonTextNext={timeUpButtonTextNext}
      timeUpText={timeUpText}
    />),
    ScnGameVideo: () => () => spec | deconstruct (({
      src,
      'button-text-next': buttonTextNext,
    }) => <ScreenGameVideo
      src={src}
      advance={screenAdvanceDispatch}
      buttonTextNext={buttonTextNext}
    />),
    ScnGameText: () => () => spec | deconstruct (({
      text,
      'text-itemlist': textItemList,
      'button-text-next': buttonTextNext,
    }) => <ScreenGameText
      buttonTextNext={buttonTextNext}
      text={text}
      textItemList={textItemList}
      advance={screenAdvanceDispatch}
    />),
    ScnGameTeamname: () => () => spec | deconstruct (({
      'button-text-next': buttonTextNext,
    }) => <ScreenGameTeamname
      advance={screenAdvanceDispatch}
      setTeamName={teamNameSetDispatch}
      buttonTextNext={buttonTextNext}
    />),
    ScnTakePhoto: () => () => spec | deconstruct (({
      'button-text-take-picture': buttonTextTakePicture,
      'text-pop-up': textPopUp,
      'button-text-accept': buttonTextAccept,
      'button-text-deny': buttonTextDeny,
    }) => lets (
      () => <ScreenTakePhoto
        buttonTextTakePicture={buttonTextTakePicture}
        textPopUp={textPopUp}
        buttonTextAccept={buttonTextAccept}
        buttonTextDeny={buttonTextDeny}
      />,
    )),
    ScnEnd: () => () => spec | deconstruct (({
      'text': text,
    }) => lets (
      () => <ScreenEnd
        text={text}
      />,
    )),
  })
)

const Main = component ('Main') (
  (props) => ({
    curScreen,
    escapeConfigFetchedDispatch,
    screenAdvanceDispatch,
    teamNameSetDispatch,
    timerStopDispatch,
  }) => {
    why ('Main', props, {
    })

    return lets (
      () => getScreenComponent ({
        spec: curScreen,
        screenAdvanceDispatch,
        teamNameSetDispatch,
        timerStopDispatch,
      }),
      (Component) => <MainS>
        <Component/>
      </MainS>,
    )
  }
)

const MainS = styled.div`
  height: 100%;
  width: 100%;
`

const mapDispatchToProps = (dispatch) => ({
  screenAdvanceDispatch: screenAdvance >> dispatch,
  timerStopDispatch: timerStop >> dispatch,
  teamNameSetDispatch: teamNameSet >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  curScreen: makeSelectCurScreen (),
})

export default Main
  | connect       (mapStateToProps, mapDispatchToProps)
