defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  ifYes,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  whenOk,
  gt,
  letS,
  ifNo,
  divideBy,
  minus,
  subtractFrom,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { Switch, Route, } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import FontFaceObserver from 'fontfaceobserver'
import memoize1 from 'memoize-one'
import Slider from 'rc-slider/lib/Slider'
import 'rc-slider/assets/index.css'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, fold, foldJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  timerSet, screenAdvance,
} from '../App/actions'

import Timer from '../Timer'

import {
  makeSelectCurTime,
  makeSelectTimerStart,
} from '../../slices/domain/selectors'

import { envIsTst, mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, inputChecked, } from '../../common'
import config from '../../config'
import escapeConfigFromJson from '../../config.json'

import {} from '../../types'

const { configGet, configGets, configGetOrDie, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const why = debugRender ? whyRender : noop

const AdminWrapperS = styled.div`
  z-index: 101;
  font-variant: normal;
  font-size: 18px;
  display: flex;
  font-family: serif;
  padding-left: 10px;
  border: 1px solid white;
  position: absolute;
  top: 50%;
  background: #462b0f;
  color: white;
  transition: left 400ms, transform 400ms;
  transform: translateX(0%);
  button {
    cursor: pointer;
    outline: none;
  }
  &.x--hidden {
    transform: translateX(calc(-100% + 50px));
  }
  .x__show-hide-button {
    flex: 1 1 auto;
    cursor: pointer;
    margin-left: 20px;
    margin-top: 0px;
    font-weight: bold;
    font-size: 20px;
    width: 50px;
    text-align: center;
    background: #412303;
    button {
      font-size: 30px;
    }
    > div {
      display: flex;
      align-items: center;
      text-align: center;
      height: 100%;
      > div {
        width: 100%;
      }
    }
  }
  .x__contents {
    flex: 1 1 auto;
    padding-top: 5px;
    padding-bottom: 10px;
    .x__button {
      text-decoration: underline;
      cursor: pointer;
    }
    .x__label {
    }
    .x__slider {
      padding-left: 5px;
    }
    .x__control, .x__label, .x__timer {
      display: inline-block;
      padding-right: 10px;
    }
  }
`

const Admin = ({
  curTime, timerStart,
  screenAdvanceDispatch, timerSetDispatch,
}) => {
  const [visible, setVisible] = useState (false)
  const toggle = useCallback (() => visible | not | setVisible, [visible])
  const cls = clss (
    visible || 'x--hidden',
  )

  return <AdminWrapperS className={cls}>
    <div className='x__contents'>
      <p>
        admin tools (tst only)
      </p>
      <div>
        <div className='x__button' onClick={screenAdvanceDispatch}>
          next page
        </div>
        <div>
          <div className='x__label'>
            cur time:
          </div>
          <div className='x__timer'>
            <Timer small={true}/>
          </div>
          <div className='x__slider'>
            <Slider
              min={0}
              max={100}
              defaultValue={curTime | divideBy (timerStart) | divideBy (1000) | multiply (100) | subtractFrom (100)}
              onChange={(amount) => 100 | minus (amount) | divideBy (100) | multiply (timerStart) | multiply (1000) | timerSetDispatch}
            />
          </div>
        </div>
      </div>
    </div>
    <div className='x__show-hide-button' onClick={toggle}>
      <div>
        <div>
          {visible ? '-' : '+'}
        </div>
      </div>
    </div>
  </AdminWrapperS>
}

const mapDispatchToProps = (dispatch) => ({
  screenAdvanceDispatch: screenAdvance >> dispatch,
  timerSetDispatch: timerSet >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  curTime: makeSelectCurTime (),
  timerStart: makeSelectTimerStart (),
})

export default Admin
  | connect       (mapStateToProps, mapDispatchToProps)
