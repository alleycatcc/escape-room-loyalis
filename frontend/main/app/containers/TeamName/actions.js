defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  tap,
} from 'stick-js/es'

import {
} from './constants'


import { logWith, } from 'alleycat-js/es/general'

