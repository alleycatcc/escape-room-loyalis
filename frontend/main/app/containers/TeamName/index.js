defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  divideBy,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {} from './actions'

import { makeSelectTeamName, } from '../../slices/domain/selectors'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, scoreAmount, } from '../../common'
import config from '../../config'

const { configGet, configGets, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const colorSecondary = 'colors.secondary' | configGet
const why = debugRender ? whyRender : noop

const TeamName = ({ teamName, }) => <TeamNameS>
   { teamName }
</TeamNameS>

const TeamNameS = styled.div`
  border: 3px solid ${colorSecondary};
  margin: auto;
  min-width: 300px;
  text-align: center;
  padding-top: 35px;
  padding-bottom: 35px;
  font-size: 45px;
  font-weight: 700;
`

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  teamName: makeSelectTeamName (),
})

export default TeamName
  | connect       (mapStateToProps, mapDispatchToProps)
