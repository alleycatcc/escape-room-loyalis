defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T, F,
  deconstruct2,
  multiply,
  divideBy,
  reject, againstAny,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, keyPressListen, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, mapX, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, withDisplayName, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  submitPicture,
  submitPictureSound,
} from '../App/actions'

import { GameHeader, Logo, Input, Dialog, } from '../../components/shared'
import { Webcam, } from '../../components/Media'
import { Text, List, Button, } from '../../containers/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, } from '../../common'
import config from '../../config'

const { configGet, configGets, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const why = debugRender ? whyRender : noop

// @todo stick
const clss = list >> reject (
  x => !x || x === true,
) >> join (' ')

const WrapperS = styled.div`
  height: 100%;
  width: 100%;
`

const colorPrimary = 'colors.primary' | configGet
const colorSecondary = 'colors.secondary' | configGet
const colorBackground = 'colors.alternate' | configGet

const TextWrapperS = styled.div`
  border: 3px solid ${colorSecondary};
  width: 80%;
  padding: 10px;
  margin: auto;
  margin-bottom: 15px;
  margin-top: -45px;
`

const InnerDialogWrapperS = styled.div`
  width: 100%;
  heigth: 100%;
  display: inline-block;
  text-align: center;
`

const ButtonsWrapperS = styled.div`
  text-align: center;
`

const ImageWrapperS = styled.div`
  padding: 30px;
  margin-top: 7%;
`
const ScreenTakePhoto = deconstructProps (
  ({
    buttonTextAccept,
    buttonTextDeny,
    buttonTextTakePicture,
    textPopUp,
    submitPictureDispatch,
    submitPictureSoundDispatch,
  }) => withDisplayName ('ScreenTakePhoto') (
    (props) => {
      why ('ScreenTakePhoto', props, {
      })

      const [picture, setPicture] = useState ()
      const [pictureDialogOpen, setPictureDialogOpen] = useState (false)
      const openPictureDialog = T >> setPictureDialogOpen
      const closePictureDialog = F >> setPictureDialogOpen

      const onCapture = (capturedPicture) => {
        setPicture (capturedPicture)
        openPictureDialog ()
      }
      const submitPicture = () => submitPictureDispatch (picture)

      return <WrapperS>
        <Dialog
          isOpen={pictureDialogOpen}
          close={closePictureDialog}
          closeOnOverlayClick={false}
          styleContent={{
            background: colorBackground,
            height: '95%',
            width: '75%',
          }}
        >
          <InnerDialogWrapperS>
            <ImageWrapperS>
              <img
                // width={810}
                // height={600}
                src={picture}
              />
            </ImageWrapperS>
            <ButtonsWrapperS>
              <Button
                onClick={submitPicture}
                size='medium'
              >
                {buttonTextAccept}
              </Button>
              <Button
                onClick={closePictureDialog}
                size='medium'
              >
                {buttonTextDeny}
              </Button>
            </ButtonsWrapperS>
          </InnerDialogWrapperS>
        </Dialog>
        <GameHeader
          showTimer={false}
          showTeamName={true}/>
        <TextWrapperS>
          <Text items={[
            `Tijd voor een foto! Neem met je team plaats voor de webcam en klik op OK als je er klaar voor bent.`,
          ]}>
          </Text>
        </TextWrapperS>
        <Webcam
          buttonTextTakePicture={buttonTextTakePicture}
          width={1000}
          height={600}
          onCapture={onCapture}
          submitPictureSound={submitPictureSoundDispatch}
        />
      </WrapperS>
    },
  ),
)

const mapDispatchToProps = (dispatch) => ({
  submitPictureDispatch: submitPicture >> dispatch,
  submitPictureSoundDispatch: submitPictureSound >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
})

export default ScreenTakePhoto
  | connect       (mapStateToProps, mapDispatchToProps)
