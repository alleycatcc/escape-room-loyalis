defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk,
} from 'stick-js/es'
