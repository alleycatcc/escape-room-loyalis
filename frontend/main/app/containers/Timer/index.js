defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  divideBy,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, ifTrueV, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { makeSelectCurTime, } from '../../slices/domain/selectors'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, } from '../../common'
import config from '../../config'

const { configGet, configGets, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const colorSecondary = 'colors.secondary' | configGet
const why = debugRender ? whyRender : noop
const fontMonospaceFamily = 'font.monospace.family' | configGet

const { floor, } = Math

const formatTime = (totalSecs) => lets (
  () => totalSecs % 60,
  () => totalSecs | divideBy (60) | floor,
  (secs, mins) => [mins, secs] | sprintfN ('%02d:%02d'),
)

const Timer = ({ curTime, animateShow, warn=false, small=false, fontSize=(small ? '16px' : '45px'), }) => lets (
  () => clss (animateShow && 'x--animate-show'),
  (cls) => <TimerS className={cls} fontSize={fontSize} small={small} warn={warn}>
    {curTime | divideBy (1000) | formatTime}
  </TimerS>,
)

const TimerS = styled.div`
  ${({ small, fontSize, }) => small | ifTrueV (`
    font-size: 16px;
  `, `
    font-size: ${fontSize};
    padding-top: 35px;
    padding-bottom: 35px;
    border: 3px solid ${colorSecondary};
  `)}
  ${prop ('warn') >> whenTrueV (`
    color: #cc3311;
  `)}
  font-family: ${fontMonospaceFamily};
  width: 48%;
  text-align: center;
  font-weight: 700;
  margin: auto;
  &.x--animate-show {
    position: relative;
    @keyframes timer-abc {
      0% {
        opacity: 0;
        left: 200px;
        top: 50px;
        width: 230px;
      }
      50% {
        opacity: 1;
      }
      100% {
        opacity: 1;
        left: 0px;
        top: 0px;
        width: 200px;
      }
    }
    animation-name: timer-abc;
    animation-duration: 500ms;
    // --- easeOutExpo
    animation-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
  }
`

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  curTime: makeSelectCurTime (),
})

export default Timer
  | connect       (mapStateToProps, mapDispatchToProps)
