// --- @todo this component has a lot of repeated code from other ones.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  // andNot, and,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
} from '../../common'

import { Logo, } from '../../components/shared'
import { Text, List, Button, } from '../../containers/shared'

import config from '../../config'

// import { } from '../../types'

import { debug, } from '../../debug'

// --- @todo alleycat-js
export const any = (...fs) => {
  const acc = []
  for (const f of fs) {
    const ret = f (...acc)
    if (ret) return ret
    acc.push (ret)
  }
  return false
}

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const IntroScreenS = styled.div`
  height: 100%;
  width: 100%;
  color: white;
`

const TitleS = styled.div`
  padding-top: 2%;
  font-size: 40px;
  font-weight: bold;
  text-align: center;
`

const LogoWrapperS = styled.div`
  padding-top: 3%;
  padding-bottom: 2%;
`

const ButtonWrapperS = styled.div`
  text-align: center;
`

const TextWrapperS = styled.div`
  width: 100%;
  padding: 1% 15% 2% 15%;
`

// --- @todo name
const ScreenTextBeforeGame = deconstructProps (
  (props) => ({
    title=null,
    text,
    textItemList,
    buttonTextNext,
    advance,
    textSecond=null,
    playOpeningSoundtrackDispatch
  }) => {
    const [items, TextComponent] = cond (
      [() => text, () => [text, Text]],
      [() => textItemList, () => [textItemList, List]],
      [T, () => die ('intro: missing both text & text-items')],
    )

    const startCountdown = useCallback (() => {
      // triggers useEffect
      setTimeLeft (3)
    })

    const [timeLeft, setTimeLeft] = useState (void 8)

    why ('ScreenTextBeforeGame', props, {
      timeLeft,
    })

    useEffect (() => {
      1000 | setTimeoutOn (timeLeft | condS ([
        // --- don't do anything on mount (timeLeft is undefined).
        nil       | guardV (noop),
        // --- don't allow the timer to ever be zero (which could cause a 0 to flash on the screen).
        1 | eq    | guardV (advance),
        // --- count down.
        otherwise | guardV (() => setTimeLeft (timeLeft - 1)),
      ]))
    },
    [timeLeft]
    )

    const CountdownS = styled.div`
      position: absolute;
      top: 50%;
      left: 50%;
      font-size: 400px;
      font-weight: bold;
      transform: translateX(-50%) translateY(-50%);
      z-index: 100;
      animation-name: slowly-appear;
      // --- the css clock and the js clock are not guaranteed in sync.
      // --- so we set this much higher than the 1 second js timeout to avoid glitches.
      // --- the keyframe gets reset on each state update.
      animation-duration: 2s;
      animation-iteration-count: 1;
      @keyframes slowly-appear {
        0% {
          opacity: 1;
          font-size: 100px;
          }
        50% {
          opacity: 0;
          font-size: 1300px;
          }
      };
    `

    return <IntroScreenS>
      <LogoWrapperS>
        <Logo />
      </LogoWrapperS>
      <TitleS>
        {title}
      </TitleS>
      <TextWrapperS>
        <TextComponent items={items}/>
        <CountdownS>
          {timeLeft}
        </CountdownS>
      </TextWrapperS>
      <ButtonWrapperS>
        <Button
          onClick={startCountdown}
          size='medium'
        >
          {buttonTextNext}
        </Button>
      </ButtonWrapperS>
      {textSecond &&  <Text items={textSecond}/>}
    </IntroScreenS>
  },
)

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
})

export default ScreenTextBeforeGame
  | connect       (mapStateToProps, mapDispatchToProps)
