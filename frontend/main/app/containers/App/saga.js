defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const MOCK = true
const MOCKDELAY = 0
const SHOW_OOPS = false

const localConfig = {
  verbose: true,
}

import {
  pipe, compose, composeRight,
  add, multiply, join,
  whenPredicate,
  path,
  sprintf1, sprintfN, ok, dot1, timesF, dot, whenOk,
  side,
  side2,
  compact,
  guard, condS,
  exception, raise,
  invoke,
  concatTo,
  lets,
  letS,
  die,
  defaultTo,
  neu1, notOk,
  drop, noop,
  ifPredicate, xMatch, id, reduceObj,
  tap, map,
  applyTo1,
  whenFalse, prop,
  ifOk,
  appendM,
  eq,
  merge, minus, reduce, mergeM,
  recurry,
  not,
  cond, guardV, otherwise, append,
  concat,
  defaultToV,
  split,
  ifAlways, nil,
  plus,
  update,
  remapTuples,
  flip,
  mapValues,
  ifNil,
  gt,
  list,
  updateM,
  assocM, T,
} from 'stick-js/es'

import { call, put, select, takeEvery, takeLatest, take, fork, } from 'redux-saga/effects'
import { eventChannel, END, } from 'redux-saga'

import { startP, allP, then, recover, } from 'alleycat-js/es/async'
import { Left, Right, fold, flatMap, liftA2, liftA2N, toJust, foldJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { trim, tellIf, error, ierror, toString, logWith, resolveP, slice1, getQueryParams, iwarn, } from 'alleycat-js/es/general'
import { letsP, } from 'alleycat-js/es/letsPromise'
import { ifEmptyString, ifEquals, } from 'alleycat-js/es/predicate'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import {
  ESCAPE_CONFIG_FETCHED,
  GAME_SCREEN_ENTERED,
  GAME_SCREEN_BONUS_ENTERED,
  HINT_BUY,
  LOGIN_TRY,
  LOGGED_IN,
  PASSWORD_ENTERED,
  PASSWORD_ENTERED_CORRECT,
  PASSWORD_ENTERED_INCORRECT,
  SCREEN_ADVANCE,
  SCREEN_ADVANCE_DO,
  SCREEN_ADVANCED,
  SCREEN_GAME_TEAMNAME_ENTERED,
  SOUND_PAUSE,
  SOUND_PLAY,
  SOUNDS_INIT,
  SUBMIT_PICTURE,
  SUBMIT_BUTTON_SOUND,
  SUBMIT_PICTURE_SOUND,
  TEXT_BEFORE_GAME_SCREEN_ENTERED,
  TIME_ELAPSED,
  TIME_UP,
  TIMER_SET,
  TIMER_START,
  TIMER_STOP,
  TIMER_UPDATED,
  UID_FETCHED,
} from '../../containers/App/constants'

import {
  gameInfoSet,
  gameScreenBonusEntered,
  gameScreenEntered,
  hintBuy,
  loggedIn,
  passwordEnteredCorrect,
  passwordEnteredIncorrect,
  scoreBonusReceived,
  scoreFinalSet,
  scoreReset,
  scoreUpdate,
  screenAdvance,
  screenAdvanceDo,
  screenAdvanced,
  screenGameTeamnameEntered,
  soundPause,
  soundPlay,
  soundsInit,
  textBeforeGameScreenEntered,
  timeElapsed,
  timeUp,
  timerSet,
  timerStart,
  timerStarted,
  timerStop,
  timerUpdated,
  uidFetched,
  updateLastScoreGained,
} from '../App/actions'

import {
  makeSelectCountdownSeverity,
  makeSelectCurScore,
  makeSelectCurScreen,
  makeSelectCurScreenType,
  makeSelectCurTime,
  makeSelectGameReward,
  makeSelectEscapeConfig,
  makeSelectFinalScore,
  makeSelectGameScreenIdx,
  makeSelectHintCost,
  makeSelectIsFirstGameScreen,
  makeSelectIsGameScreen,
  makeSelectHintsBoughtForUpload,
  makeSelectUid,
  makeSelectIsTextBeforeGameScreen,
  makeSelectIsTextBetweenGamesScreen,
  makeSelectIsGameBonusScreen,
  makeSelectIsScreenGameTeamname,
  makeSelectIsScreenTakePhoto,
  makeSelectPhotoTeam,
  makeSelectScoreStart,
  makeSelectTeamName,
  makeSelectTimerRunning,
  makeSelectTimerStart,
  makeSelectCorrectPassword,
  makeSelectBonusReceived,
} from '../../slices/domain/selectors'

import { envIsTst, envIsNotPrd, debugDev, browseLink, browseToId, orDie, putTakeN, arrow, toSha256, } from '../../common'
import config from '../../config'
import { objObjReducer, objReducer, } from '../../transform'
import { GameInfo, isSeverityNormal, } from '../../types'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const configSounds = configGetOrDie ('sounds')

// --- Saga globals. This is ok because we can assume that <App/> never gets unmounted.
let timerChan
// :: String (key): [String (src), Audio]
//    where the keys correspond to the keys of the `sounds` object in the config.
let sounds = {}

const toLower = dot ('toLowerCase')
const delay = (ms) => new Promise (
  (res, _) => setTimeout (res, ms),
)
const fromJust = toJust

const pingChannel = (ms) => eventChannel (
  (emitter) => {
    const job = setInterval (
      // --- must emit a trivial value (not nil).
      () => emitter ({}),
      ms,
    )
    return () => job | clearInterval
  },
)

const processResults = invoke (() => {
  const err = (type) => (msg) => {
    if (SHOW_OOPS) true | alertError
    error ([type, msg] | sprintfN ('%s: %s'))
  }
  return (f) => resultFold (
    f,
    (umsg) => umsg | err ('user'),
    (imsgMb) => err ('internal') (
      imsgMb | fold (id, 'no message'),
    ),
  )
})

function *requestJSONSaga (requestURL, opts) {
  const results = yield call (requestJSON, requestURL, opts)
  results | processResults (noop)
}

function *initUserSaga () {
  const requestURL = '/api/user-init'

  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
    }),
  })

  const results = yield call (requestJSON, requestURL, opts)

  const uid = results | processResults (
    path (['data', 'uid']),
  )
  // @todo temp
  | defaultToV ('unknown')

  yield uid | uidFetched | put
}

function *sagaPasswordEntered ({ data: passwordTryFull, }) {
  const passwordTry = passwordTryFull | trim | toLower

  if (envIsNotPrd && passwordTry === 'ok')
    return yield passwordEnteredCorrect () | put

  const correctPassword = yield makeSelectCorrectPassword () | select
  yield passwordTry | ifEquals (correctPassword) (
    () => passwordEnteredCorrect (),
    () => passwordEnteredIncorrect (),
  ) | put
}

function *sagaPasswordEnteredCorrect () {
  const bonus = yield makeSelectGameReward () | select
  yield updateLastScoreGained (bonus) | put
  yield scoreUpdate (bonus) | put

  /* @todo implement pingChan for bonus
  const pingChan = yield call (pingChannel, 10)

  if (bonus === 0) return

  let pingCtr = 0
  let pingCtrPrev = 0

  let points = 0
  let progress = 0
  let decelerate = 1
  let cutoff = 100

  yield takeEvery (pingChan, function *(_) {
    const pointsLeft = bonus - points
    // --- constant until the last (cutoff=100) points, then rises sharply to around 30.
    if (pointsLeft > cutoff) decelerate = 1
    else decelerate = ((cutoff - pointsLeft) / cutoff * 1.47) ** 10
    pingCtr += 1
    if (pingCtrPrev !== 0) {
      if (pingCtr - pingCtrPrev < decelerate) return
    }
    pingCtrPrev = pingCtr
    yield scoreUpdate (1) | put
    points += 1
    progress = points / bonus
    if (progress >= 1) pingChan.close ()
  })
  */
  yield screenAdvance () | put
  yield 'correct' | soundPlay | put
  yield call (delay, 1000)
  yield 'loyEarned' | soundPlay | put
}

function *sagaPasswordEnteredIncorrect () {
  yield 'incorrect' | soundPlay | put
}

function *sagaGameScreenEntered () {
  const gameScreenIdxM = yield makeSelectGameScreenIdx () | select
  const gameScreenIdx = fromJust (gameScreenIdxM)

  if (gameScreenIdx===1)  {
    yield 'openingSoundtrack' | soundPause | put
    yield 'tenzingSoundtrack' | soundPlay | put
  } else if (gameScreenIdx===2) {
    yield 'tenzingSoundtrack' | soundPause | put
    yield 'sacagaweaSoundtrack' | soundPlay | put
  } else if (gameScreenIdx===3) {
    yield 'sacagaweaSoundtrack' | soundPause | put
    yield 'worsleySoundtrack' | soundPlay | put
  }

  const curScreen = yield makeSelectCurScreen () | select
  const password = curScreen.password | orDie (
    'missing game field: password',
  )
  const hints = curScreen.hints | orDie (
    'missing game field: hints',
  )
  yield gameInfoSet (GameInfo (password, hints)) | put

  const isFirstGameScreen = yield makeSelectIsFirstGameScreen () | select
  const timerRunning = yield makeSelectTimerRunning () | select

  if (isFirstGameScreen) {
    if (timerRunning) die ('timer is already running, unexpected')
    yield timerStart () | put
    return
  }
}

function *sagaGameScreenBonusEntered () {
  const curTime = yield makeSelectCurTime () | select
  // --- @todo does this really need to be a maybe?
  const bonusMb = yield makeSelectBonusReceived () | select
  const bonus = bonusMb | fromJust | orDie (
    "The reward is not defined"
  )
  const curScore = yield makeSelectCurScore () | select
  yield curScore | plus (bonus) | scoreFinalSet | put

  const pingChan = yield call (pingChannel, 10)

  yield 'loyEarned' | soundPlay | put
  yield bonus | scoreBonusReceived | put

  if (bonus === 0) return

  let pingCtr = 0
  let pingCtrPrev = 0

  let points = 0
  let progress = 0
  let decelerate = 1
  let cutoff = 100

  yield takeEvery (pingChan, function *(_) {
    const pointsLeft = bonus - points
    // --- constant until the last (cutoff=100) points, then rises sharply to around 30.
    if (pointsLeft > cutoff) decelerate = 1
    else decelerate = ((cutoff - pointsLeft) / cutoff * 1.47) ** 10
    pingCtr += 1
    if (pingCtrPrev !== 0) {
      if (pingCtr - pingCtrPrev < decelerate) return
    }
    pingCtrPrev = pingCtr
    yield scoreUpdate (1) | put
    points += 1
    progress = points / bonus
    if (progress >= 1) pingChan.close ()
  })
}

function *sagaTextBeforeGameScreenEntered () {
  yield 'openingSoundtrack' | soundPlay | put
}


function *sagaScreenGameTeamnameEntered () {
  yield 'finaleSoundtrack' | soundPlay | put
}

function *sagaHintBuy () {
  const hintCost = yield makeSelectHintCost () | select
  yield hintCost | multiply (-1) | scoreUpdate | put
}

function *sagaTimerStart () {
  const ms = 1000
  // --- global.
  timerChan = yield call (pingChannel, ms)

  yield takeEvery (timerChan, function *(_) {
    yield ms | timeElapsed | put
  })
}

function *sagaTimerStop () {
  stopDrama ()
  yield 'worsleySoundtrack' | soundPause | put
  // --- global.
  timerChan.close ()
}

function *sagaSubmitPicture ({ data: picture, }) {
  yield screenAdvance () | put
}

function *sagaSubmitButtonSound () {
  yield 'buttonClick' | soundPlay | put
}

function *sagaSubmitPictureSound () {
  yield 'cameraSound' | soundPlay | put
}

function *sagaTimeElapsed () {
  yield timerUpdated () | put
}

function *sagaTimerSet () {
  yield timerUpdated () | put
}

const [stopDrama, startDrama] = [
  () => {
    document.body.classList.remove ('x--drama')
    // document.body.classList.remove ('x--drama-extra')
  },
  () => {
    document.body.classList.add ('x--drama')
    // document.body.classList.add ('x--drama-extra')
  },
]

const sagaTimerUpdated = invoke (() => {
  const isNotSeverityNormal = isSeverityNormal >> not
  const ifNotNormal = isNotSeverityNormal | ifPredicate

  return function * () {
    const curTime = yield makeSelectCurTime () | select
    const countdownSeverity = yield makeSelectCountdownSeverity () | select
    countdownSeverity | ifNotNormal (startDrama, stopDrama)
    if (curTime > 0) return
    stopDrama ()
    yield timeUp () | put
  }
})

function *sagaTimeUp () {
  yield timerStop () | put
  while (true) {
    const isGameScreen = yield makeSelectIsGameScreen () | select
    const isTextBetweenGamesScreen = yield makeSelectIsTextBetweenGamesScreen () | select
    if (!isGameScreen && !isTextBetweenGamesScreen) break
    yield [screenAdvance ({ automated: true, }), SCREEN_ADVANCED] | putTakeN
  }
}

function *sagaEscapeConfigFetched () {
  // --- because this is a last-minute fix, we don't want to block waiting for it or disable the
  // volgende button.
  // --- if the server is slow this could mean the uid gets set later, but we should be safe for now
  // assuming it will happen before the first game screen.
  // --- @todo block
  yield fork (initUserSaga)

  const timerStart = yield makeSelectTimerStart () | select
  const scoreStart = yield makeSelectScoreStart () | select
  yield timerStart | multiply (1000) | timerSet | put
  yield scoreStart | scoreReset | put
  yield soundsInit () | put
  yield screenAdvanced () | put

  // --- @todo @temp
  if (getQueryParams () | prop ('camera'))
    for (let i = 0; i < 18; i++)
      yield screenAdvance ({ automated: true, }) | put
}

function *sagaScreenAdvance ({ data: { automated, }}) {
  const isGameScreen = yield makeSelectIsGameScreen () | select
  const isGameBonusScreen = yield makeSelectIsGameBonusScreen () | select
  const isScreenTakePhoto = yield makeSelectIsScreenTakePhoto () | select

  const uid = yield makeSelectUid () | select
  const hints = yield makeSelectHintsBoughtForUpload () | select
  const gameIdxM = yield makeSelectGameScreenIdx () | select
  const score = yield makeSelectFinalScore () | select
  const curTime = yield makeSelectCurTime () | select
  const timerStart = yield makeSelectTimerStart () | select
  const timeElapsed = timerStart * 1000 - curTime
  const teamName = yield makeSelectTeamName () | select
  const photo = yield makeSelectPhotoTeam () | select

  const [requestURL, body] = cond (
    [
      () => isGameScreen, () => [
        '/api/hints-store',
        { uid, hints, 'game-idx': gameIdxM | fromJust },
      ],
    ],
    [
      () => isGameBonusScreen, () => [
        '/api/game-info-store',
        { uid, score, 'time-elapsed': timeElapsed },
      ],
    ],
    [
      () => isScreenTakePhoto, () => [
        '/api/team-info-store',
        { uid, photo, 'team-name': teamName },
      ],
    ],
    [T, () => []],
  )

  if (requestURL) {
    const opts = defaultOpts | merge ({
      method: 'POST',
      body: JSON.stringify (body),
    })
    // --- fork this so we don't block waiting for the api call to finish (which causes the volgende
    // button to look like it's not working).
    yield fork (requestJSONSaga, requestURL, opts)
  }

  yield screenAdvanceDo ({ automated, }) | put
}

function *sagaScreenAdvanceDo ({ data: { automated, }}) {
  yield screenAdvanced ({ automated, }) | put
}

// --- also called on the 0th screen.
function *sagaScreenAdvanced ({ data: { automated, }}) {
  if (automated) return

  const isGameScreen = yield makeSelectIsGameScreen () | select
  const isScreenTextBeforeGame = yield makeSelectIsTextBeforeGameScreen () | select
  const isGameBonusScreen = yield makeSelectIsGameBonusScreen () | select
  const isScreenGameTeamname = yield makeSelectIsScreenGameTeamname () | select

  if (isGameScreen) yield gameScreenEntered () | put
  else if (isGameBonusScreen) yield gameScreenBonusEntered () | put
  else if (isScreenTextBeforeGame) yield textBeforeGameScreenEntered () | put
  else if (isScreenGameTeamname) yield screenGameTeamnameEntered () | put
}

const sagaSoundsInit = function * () {
  sounds = configSounds | mapValues (
    prop ('src') >> arrow (
      id,
      // --- create an Audio object (which will get thrown away), so the browser starts the download.
      (src) => new Audio (src),
      (src, audio) => ({ src, audio }),
    ),
  )
}

const [sagaSoundPlay, sagaSoundPause] = invoke (() => {
  const play = 'play' | side
  const pause = 'pause' | side
  const listen = 'addEventListener' | side2
  const whenGreaterThan = gt >> whenPredicate

  // --- `duration` can be nil, in which case we play the whole sound.
  // --- returns the Audio object.
  const playWithDuration = (duration) => (src) => lets (
    // --- make a new Audio object so we can re-initialise listeners, but it still uses the sound
    // which the browser cached.
    () => new Audio (src),
    (audio) => audio | listen ('timeupdate', () => duration | whenOk (
      (ms) => audio.currentTime | multiply (1000) | whenGreaterThan (ms) (
        () => audio | pause,
      ),
    )),
    (audio) => audio | play,
  )

  // --- `f` will take a { src, audio, } object as input.
  const soundInteract = recurry (2) (
    (soundKey) => (f) => sounds [soundKey] | ifNil (
      () => die (soundKey | sprintf1 ('bad key: %s, forgot soundsInit?')),
      f,
    )
  )

  return [
    function * ({ data: { soundKey, duration=null, }}) {
      sounds | assocM (
        soundKey,
        soundInteract (
          soundKey,
          prop ('src') >> arrow (
            id,
            playWithDuration (duration),
            (src, audio) => ({ src, audio }),
          ),
        ),
      )
    },
    function * ({ data: soundKey, }) {
      soundInteract (
        soundKey,
        prop ('audio') >> pause,
      )
    },
  ]
})

function *sagaLoggedIn () {
}

function *sagaLoginTry ({ data: passwordTry, }) {

  // --- to generate:
  // toSha256 ('<some-password>') | then (neu1 (Uint8Array) >> tap (logWith ('password')))

  const correctPassword = new Uint8Array ([
    102, 119, 188, 204, 168, 202, 43, 61, 114, 37, 155, 250, 139, 58, 77, 64, 150, 216, 230, 228,
    106, 235, 91, 115, 86, 128, 169, 110, 225, 150, 224, 251,
  ])

  const ok = yield recover (
    (err) => false | tap (() => ierror (err)),
    letsP (
      () => toSha256 (passwordTry),
      (sha) => new Uint8Array (sha),
      (_, tryPass) => tryPass.toString () === correctPassword.toString (),
    ),
  )
  if (!ok) return
  yield loggedIn () | put
  yield screenAdvance () | put
}

function *sagaUidFetched ({ data: uid, }) {
}

export default function *rootSaga () {
  yield [
    takeLatest (ESCAPE_CONFIG_FETCHED, sagaEscapeConfigFetched),
    takeLatest (GAME_SCREEN_ENTERED, sagaGameScreenEntered),
    takeLatest (GAME_SCREEN_BONUS_ENTERED, sagaGameScreenBonusEntered),
    takeLatest (HINT_BUY, sagaHintBuy),
    takeLatest (LOGIN_TRY, sagaLoginTry),
    takeLatest (LOGGED_IN, sagaLoggedIn),
    takeLatest (PASSWORD_ENTERED, sagaPasswordEntered),
    takeLatest (PASSWORD_ENTERED_CORRECT, sagaPasswordEnteredCorrect),
    takeLatest (PASSWORD_ENTERED_INCORRECT, sagaPasswordEnteredIncorrect),
    takeLatest (SCREEN_ADVANCE, sagaScreenAdvance),
    takeLatest (SCREEN_ADVANCE_DO, sagaScreenAdvanceDo),
    takeLatest (SCREEN_ADVANCED, sagaScreenAdvanced),
    takeLatest (SCREEN_GAME_TEAMNAME_ENTERED, sagaScreenGameTeamnameEntered),
    takeLatest (SOUND_PAUSE, sagaSoundPause),
    takeLatest (SOUND_PLAY, sagaSoundPlay),
    takeLatest (SOUNDS_INIT, sagaSoundsInit),
    takeLatest (SUBMIT_PICTURE, sagaSubmitPicture),
    takeLatest (SUBMIT_BUTTON_SOUND, sagaSubmitButtonSound),
    takeLatest (SUBMIT_PICTURE_SOUND, sagaSubmitPictureSound),
    takeLatest (TEXT_BEFORE_GAME_SCREEN_ENTERED, sagaTextBeforeGameScreenEntered),
    takeLatest (TIME_ELAPSED, sagaTimeElapsed),
    takeLatest (TIME_UP, sagaTimeUp),
    takeLatest (TIMER_SET, sagaTimerSet),
    takeLatest (TIMER_START, sagaTimerStart),
    takeLatest (TIMER_STOP, sagaTimerStop),
    takeLatest (TIMER_UPDATED, sagaTimerUpdated),
    takeLatest (UID_FETCHED, sagaUidFetched),
  ]
}
