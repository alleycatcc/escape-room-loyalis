defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  ifYes,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  whenOk,
  gt,
  letS,
  ifNo,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { Switch, Route, } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import FontFaceObserver from 'fontfaceobserver'
import memoize1 from 'memoize-one'
import Slider from 'rc-slider/lib/Slider'
import 'rc-slider/assets/index.css'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, fold, foldJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, setTimeoutOn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { escapeConfigFetched, screenAdvance, sliceDomainReplace, } from './actions'

import domainReducer, { pickleSpec as domainReducerPickleSpec, } from '../../slices/domain/reducer'
import uiReducer from '../../slices/ui/reducer'
import appReducer from '../../slices/app/reducer'

import {
  makeSelectError,
  makeSelectHasEscapeConfig,
  makeSelectMainSoundKey,
  makeSelectShowAdminToolsAllowed,
} from '../../slices/domain/selectors'

import saga from './saga'

import { Alert, } from '../../alleycat-components'

import {
  pickle, unpickle,
  startUnpickle, finishPickle,
} from '../../pickle/pickle'

import { Sound, } from '../../components/Media'

import Admin from '../../containers/Admin'
import Main from '../../containers/Main'
import NotFoundPage from '../../containers/NotFoundPage'

import { envIsTst, mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, } from '../../common'
import config from '../../config'
import escapeConfigFromJson from '../../config.json'

import {} from '../../types'

import injectReducer from '../../utils/injectReducer'
import injectSaga from '../../utils/injectSaga'

const { configGet, configGets, configGetOrDie, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const why = debugRender ? whyRender : noop

const App = deconstructProps (
  (props) => ({
    error, hasEscapeConfig,
    mainSoundKey,
    screenAdvanceDispatch,
    escapeConfigFetchedDispatch,
    showAdminToolsAllowed,

    // @temp
    sliceDomainReplaceDispatch,
    __store__,
  }) => {
    why ('App', props, {
    })

    useEffect (() => {
      escapeConfigFromJson | escapeConfigFetchedDispatch
    }, [])

    if (false) useEffect (() => {
      10000 | setTimeoutOn (() => {
        const store = __store__.getState ().domain
        const stored = store | pickle (domainReducerPickleSpec) | finishPickle
        const unpickled = stored | startUnpickle | unpickle (domainReducerPickleSpec)
        console.log ('stored', stored)
        unpickled.curTime = 42000
        unpickled.hintsBought.pop ()
        sliceDomainReplaceDispatch (unpickled)
      })
    }, [])

    return <AppWrapperS>
      {cond (
        [() => error, () => 'Sorry, something has gone wrong.'],
        [() => hasEscapeConfig | not, () => null],
        [T, () => <Fragment>
          {showAdminToolsAllowed && <Admin/>}
          <Main/>
        </Fragment>,
        ],
      )}
      <Alert />
    </AppWrapperS>
})

const AppWrapperS = styled.div`
  height: 100%;
  overflow-y: hidden;
`

const mapDispatchToProps = (dispatch) => ({
  escapeConfigFetchedDispatch: escapeConfigFetched >> dispatch,
  screenAdvanceDispatch: screenAdvance >> dispatch,

  // @temp
  sliceDomainReplaceDispatch: sliceDomainReplace >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  error: makeSelectError (),
  hasEscapeConfig: makeSelectHasEscapeConfig (),
  mainSoundKey: makeSelectMainSoundKey (),
  showAdminToolsAllowed: makeSelectShowAdminToolsAllowed (),
})

export default App
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'app' })
  | injectReducer ({ reducer: domainReducer, key: 'domain' })
  | injectReducer ({ reducer: uiReducer,     key: 'ui' })
  | injectReducer ({ reducer: appReducer,    key: 'app' })
