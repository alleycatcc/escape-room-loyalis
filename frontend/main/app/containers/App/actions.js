defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  tap,
} from 'stick-js/es'

import {
  ADMIN_TOOLS_DISPLAY,
  ESCAPE_CONFIG_FETCHED,
  GAME_INFO_SET,
  GAME_SCREEN_BONUS_ENTERED,
  GAME_SCREEN_ENTERED,
  HINT_BUY,
  HINT_TAKE,
  LAST_PASSWORD_ENTERED_CLEAR,
  LOGIN_TRY,
  LOGGED_IN,
  PASSWORD_ENTERED,
  PASSWORD_ENTERED_CORRECT,
  PASSWORD_ENTERED_INCORRECT,
  SCORE,
  SCORE_BONUS_RECEIVED,
  SCORE_FINAL_SET,
  SCORE_RESET,
  SCORE_UPDATE,
  SCREEN_ADVANCE,
  SCREEN_ADVANCE_DO,
  SCREEN_ADVANCED,
  SCREEN_GAME_TEAMNAME_ENTERED,
  SLICE_DOMAIN_REPLACE,
  SOUND_PAUSE,
  SOUND_PLAY,
  SOUNDS_INIT,
  SUBMIT_BUTTON_SOUND,
  SUBMIT_PICTURE,
  SUBMIT_PICTURE_SOUND,
  TEAM_NAME_SET,
  TEXT_BEFORE_GAME_SCREEN_ENTERED,
  TIME_ELAPSED,
  TIME_UP,
  TIMER_SET,
  TIMER_START,
  TIMER_STARTED,
  TIMER_STOP,
  TIMER_UPDATED,
  UID_FETCHED,
  UPDATE_LAST_SCORE_GAINED,
} from './constants'

import { Nothing, Just, } from 'alleycat-js/es/bilby'
import { logWith, } from 'alleycat-js/es/general'

// --- `automated` is `true` when the screens are being automatically advanced in a saga (currently
// useful for the time up action to advance through all the game screens without playing a sound).

export const screenAdvance = ({ automated=false, }={}) => ({
  type: SCREEN_ADVANCE,
  data: { automated, },
})

export const screenAdvanceDo = ({ automated=false, }={}) => ({
  type: SCREEN_ADVANCE_DO,
  data: { automated, },
})

// --- `automated`: see `screenAdvance`
export const screenAdvanced = ({ automated=false, }={}) => ({
  type: SCREEN_ADVANCED,
  data: { automated, },
})

export const sliceDomainReplace = (slice) => ({
  type: SLICE_DOMAIN_REPLACE,
  data: slice,
})

export const gameScreenEntered = () => ({
  type: GAME_SCREEN_ENTERED,
})

  /*
export const hintBuy = (path) => ({
  type: HINT_BUY,
  data: path,
})
*/

export const hintBuy = () => ({
  type: HINT_BUY,
})

export const hintTake = (categoryIdx) => ({
  type: HINT_TAKE,
  data: categoryIdx,
})

export const gameScreenBonusEntered = () => ({
  type: GAME_SCREEN_BONUS_ENTERED,
})

export const gameInfoSet = (gameInfo) => ({
  type: GAME_INFO_SET,
  data: gameInfo,
})

export const passwordEntered = (passwordTry) => ({
  type: PASSWORD_ENTERED,
  data: passwordTry,
})

export const passwordEnteredCorrect = () => ({
  type: PASSWORD_ENTERED_CORRECT,
})

export const passwordEnteredIncorrect = () => ({
  type: PASSWORD_ENTERED_INCORRECT,
})

export const lastPasswordEnteredClear = () => ({
  type: LAST_PASSWORD_ENTERED_CLEAR,
})

export const score = () => ({
  type: SCORE,
})

export const scoreBonusReceived = (amount) => ({
  type: SCORE_BONUS_RECEIVED,
  data: amount,
})

// --- 'set' as in imperative mood.
export const scoreFinalSet = (score) => ({
  type: SCORE_FINAL_SET,
  data: score,
})

export const scoreUpdate = (gamePoints) => ({
  type: SCORE_UPDATE,
  data: gamePoints
})

export const scoreReset = (resetScore) => ({
  type: SCORE_RESET,
  data: resetScore
})

// duration :: nil | Number (ms)
export const soundPlay = (soundKey, duration) => ({
  type: SOUND_PLAY,
  data: { soundKey, duration },
})

export const soundPause = (soundKey) => ({
  type: SOUND_PAUSE,
  data: soundKey,
})

export const soundsInit = () => ({
  type: SOUNDS_INIT,
})

export const submitButtonSound = () => ({
  type: SUBMIT_BUTTON_SOUND,
})

export const submitPicture = (picture) => ({
  type: SUBMIT_PICTURE,
  data: picture,
})

export const submitPictureSound = () => ({
  type: SUBMIT_PICTURE_SOUND,
})
export const timerSet = (ms) => ({
  type: TIMER_SET,
  data: ms,
})

export const timerStart = () => ({
  type: TIMER_START,
})

export const timerStop = () => ({
  type: TIMER_STOP,
})

export const timeElapsed = (ms) => ({
  type: TIME_ELAPSED,
  data: ms,
})

export const timeUp = () => ({
  type: TIME_UP,
})

export const timerStarted = (channel) => ({
  type: TIMER_STARTED,
  data: channel,
})

export const timerUpdated = () => ({
  type: TIMER_UPDATED,
})

export const adminToolsDisplay = (onOff) => ({
  type: ADMIN_TOOLS_DISPLAY,
  data: onOff,
})

export const escapeConfigFetched = (config) => ({
  type: ESCAPE_CONFIG_FETCHED,
  data: config,
})

export const textBeforeGameScreenEntered = () => ({
  type: TEXT_BEFORE_GAME_SCREEN_ENTERED,
})

export const screenGameTeamnameEntered = () => ({
  type: SCREEN_GAME_TEAMNAME_ENTERED,
})

export const teamNameSet = (name) => ({
  type: TEAM_NAME_SET,
  data: name,
})

export const loginTry = (password) => ({
  type: LOGIN_TRY,
  data: password,
})

export const loggedIn = () => ({
  type: LOGGED_IN,
})

export const updateLastScoreGained = (amount) => ({
  type: UPDATE_LAST_SCORE_GAINED,
  data: amount
})

export const uidFetched = (uid) => ({
  type: UID_FETCHED,
  data: uid,
})
