defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T,
  deconstruct2,
  multiply,
  divideBy,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {} from './actions'

import { makeSelectCurScore, makeSelectCurTime, } from '../../slices/domain/selectors'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, scoreAmountList, } from '../../common'
import config from '../../config'

const { configGet, configGets, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const colorSecondary = 'colors.secondary' | configGet
const why = debugRender ? whyRender : noop
const fontMonospaceFamily = 'font.monospace.family' | configGet

const { floor, } = Math

const Score = ({ curScore, animateShow, }) => lets (
  () => clss (animateShow && 'x--animate-show'),
  () => curScore | scoreAmountList,
  (cls, [amount, units]) => <ScoreS className={cls}>
    <div className='x__amount'>
      {amount}
    </div>
    <div className='x__units'>
      {units}
    </div>
  </ScoreS>,
)

const ScoreS = styled.div`
  border: 3px solid ${colorSecondary};
  width: 50%;
  margin: auto;
  text-align: center;
  padding-top: 35px;
  padding-bottom: 35px;
  font-size: 45px;
  font-weight: 700;
  .x__amount {
    font-family: ${fontMonospaceFamily};
    display: inline-block;
  }
  .x__units {
    padding-left: 10px;
    display: inline-block;
  }
  &.x--animate-show {
    position: relative;
    @keyframes score-abc {
      0% {
        opacity: 0;
        right: 20px;
        top: 100px;
        width: 350px;
      }
      50% {
        opacity: 1;
      }
      100% {
        opacity: 1;
        right: 0px;
        top: 0px;
        width: 300px;
      }
    }
    animation-name: score-abc;
    animation-duration: 300ms;
    // --- easeOutExpo
    animation-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
  }
`

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  curScore: makeSelectCurScore (),
  curTime: makeSelectCurTime (),
})

export default Score
  | connect       (mapStateToProps, mapDispatchToProps)
