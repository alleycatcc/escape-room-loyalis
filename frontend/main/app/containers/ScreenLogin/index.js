defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  xReplace,
  flip,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useCallback, useEffect, uonOffseCallback, useLayoutEffect, } from 'react'

import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { then, recover, allP, startP, } from 'alleycat-js/es/async'
import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, keyPressListen, inputValue, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
  useMeasure,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  component, loy,
  inputChecked,
} from '../../common'

import {
  adminToolsDisplay,
  loginTry,
} from '../App/actions'

import { makeSelectShowAdminTools, } from '../../slices/domain/selectors'

import { Logo, Input, } from '../../components/shared'
import { Text, Button, } from '../../containers/shared'

import config from '../../config'

import {} from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const WrapperS = styled.div`
  height: calc(100% - 100px);
  width: 100%;
  .x__logo {
    margin: auto;
  }
`

const ButtonWrapperS = styled.div`
  text-align: center;
  font-size: 20px;
  margin-top: 60px;
`

const LoginWrapperS = styled.div`
  background: #462b0f;
  font-family: serif;
  border: 1px solid #999999;
  width: 60%;
  margin: auto;
  margin-top: 100px;
  font-size: 20px;
  padding: 10px;
  div {
  }
  input {
    position: relative;
    top: 2px;
  }
  .x__controls {
    margin-top: 30px;
  }
  .x__welkom {
    font-size: 40px;
  }
  .x__label {
    display: inline-block;
    margin-left: 5px;
  }
  .x__input {
    margin-top: 10px;
  }
`

const ScreenLogin = ({
  showAdminTools,
  adminToolsDisplayDispatch,
  loginTryDispatch,
}) => {
  const [password, setPassword] = useState ()
  const onSetAdminTools = useCallbackConst (adminToolsDisplayDispatch)
  const onSubmit = useCallback (
    () => password | loginTryDispatch,
    [password],
  )
  const onKeyPress = keyPressListen (
    onSubmit,
    'Enter',
  )
  const onChange = inputValue >> setPassword

  return <WrapperS>
    <div className='x__logo'>
      <Logo/>
    </div>
    <LoginWrapperS>
      <div className='x__welkom'>
        Welkom.
      </div>
      <div className='x__controls'>
        <div>
          <label>
            <input
              type='checkbox'
              defaultChecked={showAdminTools}
              onChange={inputChecked >> onSetAdminTools}
            />
            <div className='x__label'>
              toon admin tools
            </div>
          </label>
        </div>
        <div>
          <input
            type='checkbox'
            disabled={true}
            defaultChecked={false}
          />
          <div className='x__label'>
            herstel vorige speelsessie (work in progress)
          </div>
        </div>
        <div className='x__input'>
          <Input
            type='password'
            placeholder='admin wachtwoord'
            onChange={onChange}
            onKeyPress={onKeyPress}
            style={{
              paddingBottom: '15px',
              height: '53px',
            }}
          />
        </div>
      </div>
    </LoginWrapperS>
  </WrapperS>
}

const mapDispatchToProps = (dispatch) => ({
  loginTryDispatch: loginTry >> dispatch,
  adminToolsDisplayDispatch: adminToolsDisplay >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  showAdminTools: makeSelectShowAdminTools (),
})

export default ScreenLogin
  | connect       (mapStateToProps, mapDispatchToProps)
