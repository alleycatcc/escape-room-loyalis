defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  condS, guard, eq, otherwise,
  cond, not, T, F,
  deconstruct2,
  multiply,
  divideBy,
  reject, againstAny,
  xReplace,
  plus,
  recurry,
  ifPredicate,
  last,
  minus,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel, } from 'react-accessible-accordion';

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import { cata, foldJust, fold, isJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, keyPressListen, } from 'alleycat-js/es/dom'
import { logWith, info, warn, iwarn, mapX, length, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, isEmptyList, } from 'alleycat-js/es/predicate'
import { deconstructProps, withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, whyRender, withDisplayName, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  makeSelectCanBuyAHint,
  makeSelectCountdownSeverity,
  makeSelectHintCategoryData,
  makeSelectLastPasswordEntered,
  makeSelectIsFirstGameScreen,
  makeSelectHintText,
  makeSelectHintCost,
  makeSelectHintsBought,
} from '../../slices/domain/selectors'

import {
  hintBuy,
  hintTake,
  lastPasswordEnteredClear,
  passwordEntered,
} from '../App/actions'

import { GameHeader, Logo, Input, Dialog, GameWrapperS, } from '../../components/shared'
import { Sound, } from '../../components/Media'
import { Text, List, Button, } from '../../containers/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, component, fromJust, scoreAmount, orDie, untag, fold4, whenNotEmptyListV, } from '../../common'
import config from '../../config'
import { passwordIncorrect, isSeverityNormal, } from '../../types'

const { configGet, configGets, configGetOrDie, configGetsOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const why = debugRender ? whyRender : noop

const gameImages = 'images.gameImages' | configGetOrDie

const increment = 1 | plus
const isNotSeverityNormal = isSeverityNormal >> not

// @todo alleycat-js
const clss = list >> reject (
  x => !x || x === true,
) >> join (' ')

const AccordionWrapperS = styled.div`
  position: relative;
  .accordion {
    padding: 20px;
    min-width: 300px;
    font-size: 30px;
  }
  .accordion__heading {
  }
  .accordion__item {
    font-size: 20px;
    border: 1px solid black;
  }
  .accordion__button {
    cursor: pointer;
    border: none;
    padding: 5px;
    background-color: #202020;
  }
  .accordion__button:hover {
    background-color: #121212;
  }
  .accordion__button:before {
      display: inline-block;
      content: '';
      height: 10px;
      width: 10px;
      margin-right: 12px;
      border-bottom: 2px solid currentColor;
      border-right: 2px solid currentColor;
      transform: rotate(-45deg);
  }

  .accordion__button[aria-expanded='true']::before,
  .accordion__button[aria-selected='true']::before {
      transform: rotate(45deg);
  }
  .accordion__panel {
    padding: 15px;
    background-color: #323232;
    animation: fadein 0.45s ease-in;
  }
  @keyframes fadein {
      0% {
          opacity: 0;
      }
      100% {
          opacity: 1;
      }
  }
`
const ButtonWrapperS = styled.div`
  display: flex;
  justify-content: space-between;
  width: 66%;
  margin-left: 17%;
  margin-top: 3%;
  height: 50px;
  .x__yes-button {
    margin-left: 15%;
  }
  .x__no-button {
    margin-right: 15%;
  }
`

const TextWrapperS = styled.div`
  margin-top: 7%;
  padding: 20px;
`

const InputWrapperS = styled.div`
  width: 66%;
  margin-left: 17%;
  margin-top: 10px;
  margin-bottom: 10px;
  height: 60px;
`

const HintImg = styled.img`
  padding: 10px;
`

const GameViewS = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 17%;
  margin-right: 17%;

  // --- @todo ugly
  overflow-y: auto;
  overflow-x: hidden;
  height: 460px;
  padding-bottom: 20px;

  .x__item {
    min-height: 300px;
    &:nth-child(1) {
      flex: 1 0 auto;
      max-width: 70%;
      padding-right: 100px;
    }
    &:nth-child(2) {
      flex: 0 1 auto;
      img {
        float: right;
      }
    }
  }
`

const GameView = ({ text, image, }) => <GameViewS>
  <div className='x__item'>
    <Text items={text}/>
  </div>
  <div className='x__item'>
    <img src={image}/>
  </div>
</GameViewS>

const colorPrimary = 'colors.primary' | configGet
const colorSecondary = 'colors.secondary' | configGet
const colorBackground = 'colors.alternate' | configGet

const HintsS = styled.div`
  text-align: center;
  padding: 3%;
  color: ${colorPrimary};
  background: ${colorBackground};
  font-size: 100%;
  height: 100%;
  width: 100%;
  .x__item {
    width: 100%;
    display: inline-block;
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 7px;
    cursor: pointer;
    &.x--disabled {
      opacity: 0.6;
      cursor: inherit;
    }
  }
`

const HintContents = ({ hintCategoryDataMb, hintText, hintCost, takeHint, buyHint, close, openHintsBoughtDialog, }) => {
  const [page, setPage] = useState (0)
  const nextPage = useCallback (() => page | increment | setPage, [page])
  const onBuy = useCallback (() => {
    buyHint ()
    nextPage ()
  }, [nextPage])
  const onTake = useCallback ((categoryIdx) => {
    takeHint (categoryIdx)
    openHintsBoughtDialog ()
    close ()
  })

  const pages = [
    // --- confirm page
    <div key={0} className='x__confirm'>
      <TextWrapperS>
        <Text animateTextProp={false} items={hintText['1']}/>
        <Text animateTextProp={false} items={hintText['2'] | map (xReplace (
          /__LOY__/, hintCost | scoreAmount,
        ))}/>
        <Text animateTextProp={false} items={hintText['3']}/>
      </TextWrapperS>
      <ButtonWrapperS>
        <Button
          className="x__yes-button"
          onClick={onBuy}
          size='small'
        >
          {hintText.ja}
        </Button>
        <Button
          className="x__no-button"
          onClick={close}
          size='small'
        >
          {hintText.nee}
        </Button>
      </ButtonWrapperS>
    </div>,

    // --- category page
    hintCategoryDataMb | mapX (
      ({ name, canBuy }, categoryIdx) => <div
        key={categoryIdx}
        className='x__item'
      >
        <Button
          size='large'
          disabled={canBuy | not}
          onClick={() => onTake (categoryIdx)}
        >
          {name}
        </Button>
      </div>,
    ),
  ]

  return <HintsS>
    {pages [page] | orDie ('bad page: ' + String (page))}
  </HintsS>
}

const HintsBoughtS = styled.div`
  position: absolute;
  color: black;
  top: 0px;
  right: 0px;
  background: ${colorPrimary};
`

const ScreenGame = component ('ScreenGame') (
  (props) => ({
    buttonTextSubmit,
    buttonTextHint,
    passwordPlaceholder,
    advance,
    text,
    image,
    isFirstGameScreen,
    hintText,
    hintCost,
    hintsBought,
    canBuyAHint,
    lastPasswordEntered,
    hintCategoryDataMb,
    countdownSeverity,
    hintBuyDispatch,
    hintTakeDispatch,
    passwordEnteredDispatch,
    lastPasswordEnteredClearDispatch,
  }) => {
    why ('ScreenGame', props, {
    })

    // Temporay buttonTexts
    const buttonTextAvailableHints = "Beschikbare hints"
    const buttonTextBuyHints = "Vraag een hint"

    const [passwordTry, setPasswordTry] = useState ("")

    const lastPasswordIncorrect = lastPasswordEntered | passwordIncorrect

    const submitPassword = useCallback (
      () => passwordEnteredDispatch (passwordTry),
      [passwordEnteredDispatch, passwordTry],
    )

    const onKeyPress = keyPressListen (
      submitPassword,
      'Enter',
    )

    const onChange = (event) => {
      setPasswordTry (event.target.value)
      lastPasswordEnteredClearDispatch ()
    }
    const [hintsBuyDialogOpen, setHintsBuyDialogOpen] = useState (false)
    const openHintsBuyDialog = useCallback (T >> setHintsBuyDialogOpen)
    const closeHintsBuyDialog = useCallback (F >> setHintsBuyDialogOpen)

    const [hintsBoughtDialogOpen, setHintsBoughtDialogOpen] = useState (false)
    const openHintsBoughtDialog = useCallback (T >> setHintsBoughtDialogOpen)
    const closeHintsBoughtDialog = useCallback (F >> setHintsBoughtDialogOpen)

    return <GameWrapperS countdownSeverity={countdownSeverity}>
      <Dialog
        isOpen={hintsBuyDialogOpen}
        close={closeHintsBuyDialog}
        closeOnOverlayClick={false}
        styleContent={{
          background: colorBackground,
        }}
      >
        <HintContents
          hintText={hintText}
          hintCost={hintCost}
          hintCategoryDataMb={hintCategoryDataMb}
          takeHint={hintTakeDispatch}
          buyHint={hintBuyDispatch}
          close={closeHintsBuyDialog}
          openHintsBoughtDialog={openHintsBoughtDialog}
        />
      </Dialog>
      <Dialog
        isOpen={hintsBoughtDialogOpen}
        close={closeHintsBoughtDialog}
        closeOnOverlayClick={true}
        styleContent={{
          background: colorBackground,
        }}
      >
        <AccordionWrapperS>
          <Accordion
            className='accordion'
            allowZeroExpanded={false}
            preExpanded={[hintsBought | length | minus (1) | String]}
          >
            {hintsBought | whenNotEmptyListV ('Hints')}
            {hintsBought | mapX (
              (hint, idx) => hint | untag (
                (category, text, imageMb) => <AccordionItem
                  uuid={String(idx)}
                  className='accordion__item'
                  key={idx}
                >
                  <AccordionItemHeading className='accordion__heading'>
                    <AccordionItemButton className='accordion__button'>
                      <Text
                        items={[ category ]}
                        align='left'
                        display='inline-block'
                        animateTextProp={false}
                      />
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel>
                    <Text items={text} align='left'/>
                    {imageMb | foldJust (
                      (imageKey) => lets (
                        () => gameImages [imageKey] || die ('No image for key: ' + imageKey),
                        (image) => <HintImg src={image}/>,
                      ),
                    )}
                  </AccordionItemPanel>
                </AccordionItem>,
              ),
            )}
          </Accordion>
        </AccordionWrapperS>
      </Dialog>
      <div className='x__bg-overlay'/>
      <GameHeader
        animateScore={isFirstGameScreen}
        animateLogo={isFirstGameScreen}
        animateTimer={isFirstGameScreen}
        warnTimer={countdownSeverity | isNotSeverityNormal}
      />
      <GameView
        text={text}
        image={image}
        hintsBought={hintsBought}
      />
      <InputWrapperS>
        <Input
          height={'100%'}
          width={'100%'}
          className={clss (lastPasswordIncorrect && 'x--error')}
          onKeyPress={onKeyPress}
          onChange={onChange}
          placeholder={passwordPlaceholder}
        />
      </InputWrapperS>
      <ButtonWrapperS>
        <Button
          onClick={openHintsBoughtDialog}
          disabled={hintsBought | isEmptyList }
          size='wide'
          wide={true}
        >
          {buttonTextAvailableHints}
        </Button>
        <Button
          onClick={submitPassword}
          size='wide'
          wide={true}
        >
          {buttonTextSubmit}
        </Button>
        <Button
          onClick={openHintsBuyDialog}
          disabled={canBuyAHint | not}
          size='wide'
          wide={true}
        >
          {buttonTextBuyHints}
        </Button>
      </ButtonWrapperS>
    </GameWrapperS>
  }
)

const mapDispatchToProps = (dispatch) => ({
  hintBuyDispatch: hintBuy >> dispatch,
  hintTakeDispatch: hintTake >> dispatch,
  lastPasswordEnteredClearDispatch: lastPasswordEnteredClear >> dispatch,
  passwordEnteredDispatch: passwordEntered >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  canBuyAHint: makeSelectCanBuyAHint (),
  hintCategoryDataMb: makeSelectHintCategoryData (),
  lastPasswordEntered: makeSelectLastPasswordEntered (),
  isFirstGameScreen: makeSelectIsFirstGameScreen (),
  hintText: makeSelectHintText (),
  hintCost: makeSelectHintCost (),
  hintsBought: makeSelectHintsBought (),
  countdownSeverity: makeSelectCountdownSeverity (),
})

export default ScreenGame
  | connect       (mapStateToProps, mapDispatchToProps)
