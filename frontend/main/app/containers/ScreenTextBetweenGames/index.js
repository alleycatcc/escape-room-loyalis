defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  xReplace,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop, loy
} from '../../common'

import {
  makeSelectLastScoreGained,
  makeSelectCountdownSeverity,
} from '../../slices/domain/selectors'

import { Logo, GameHeader, GameWrapperS, } from '../../components/shared'
import { Text, List, Button, } from '../../containers/shared'

import config from '../../config'

// import { } from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const IntroScreenS = styled.div`
  height: 100%;
  width: 100%;
  color: white;
`

const TitleS = styled.div`
  font-size: 40px;
  font-weight: bold;
  text-align: center;
`

const ButtonWrapperS = styled.div`
  text-align: center;
`

const LogoWrapperS = styled.div`
  padding-top: 3%;
  padding-bottom: 3%;
`

const TextWrapperS = styled.div`
  width: 100%;
  padding: 9% 15% 2% 15%;
`

const ScreenTextBetweenGames = deconstructProps (
  (props) => ({
    title=null,
    text,
    textItemList,
    buttonTextNext,
    advance,
    textSecond=null,
    lastScoreGained,
    countdownSeverity,
  }) => {
    why ('ScreenIntro', props, {
    })

    const loyForm = lastScoreGained | loy
    const [items, TextComponent] = cond (
      [() => text, () => [text | map (
        xReplace (/__POINTS__/, lastScoreGained)
        >> xReplace (/__LOY__/, loyForm)
      ), Text]],
      [() => textItemList, () => [textItemList, List]],
      [T, () => die ('intro: missing both text & text-items')],
    )

    return <GameWrapperS countdownSeverity={countdownSeverity}>
      <div className='x__bg-overlay'/>
      <GameHeader/>
      <TitleS>
        {title}
      </TitleS>
      <TextWrapperS>
        <TextComponent items={items}/>
      </TextWrapperS>
      <ButtonWrapperS>
        <Button
          onClick={advance}
          size='medium'
        >
          {buttonTextNext}
        </Button>
      </ButtonWrapperS>
      {textSecond &&  <Text items={textSecond}/>}
    </GameWrapperS>
  },
)

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  lastScoreGained: makeSelectLastScoreGained (),
  countdownSeverity: makeSelectCountdownSeverity (),
})

export default ScreenTextBetweenGames
  | connect       (mapStateToProps, mapDispatchToProps)
