defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
  whenTrue, merge,
  ne, whenPredicate,
  recurry,
  die,
  arg2, defaultTo,
  sprintf1,
  nil, ampersandN,
  add, multiply,
  xReplace,
  flip,
} from 'stick-js/es'

import React, { Fragment, memo, useState, useRef, useEffect, useCallback, useLayoutEffect, } from 'react'

import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import { Nothing, Just, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, infoWith, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, info, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, whenEquals, isEmptyObject, all, allV, ifAllN, } from 'alleycat-js/es/predicate'
import {
  whyRender, shouldUpdate, deconstructProps, usePrevious, useCallbackConst,
  useRefSet, useMeasureStore, useMeasureWithStore, useMeasureWithAfter, memoIgnore,
  useMeasure,
} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  component, loy,
} from '../../common'

import {
  makeSelectBonusReceived,
  makeSelectTimeUp,
  makeSelectCurScore,
} from '../../slices/domain/selectors'

import { Logo, GameHeader, } from '../../components/shared'
import { Text, Button, } from '../../containers/shared'

import Score from '../../containers/Score'
import Timer from '../../containers/Timer'

import config from '../../config'

import {} from '../../types'

import { debug, } from '../../debug'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const debugRender = 'debug.render' | configGet
const logo = 'images.logo' | configGet
const why = debugRender ? whyRender : noop

const WrapperS = styled.div`
  height: calc(100% - 100px);
  width: 100%;
`

// --- @todo duplicated from ScreenGame
const HeaderS = styled.div`
  display: flex;
  margin-bottom: 100px;
  .x__item {
    flex: 1 1 auto;
    &:nth-child(1) {
      text-align: center;
      * {
        margin: auto;
      }
    }
    &:nth-child(2) {
    }
    &:nth-child(3) {
      * {
        margin: auto;
        width: 200px;
      }
    }
  }
`

// --- @todo duplicated from ScreenGame
const ButtonWrapperS = styled.div`
  text-align: center;
  font-size: 80px;
  margin-top: 60px;
`

const ScreenBonus = component ('ScreenBonus') (
  (props) => ({
    advance,
    bonusReceived,
    curScore,
    timeUp,
    timeRemainingButtonTextNext,
    timeRemainingText,
    timeUpButtonTextNext,
    timeUpText,
  }) => {
    return bonusReceived | flip (fold) (
      null,
      (bonusAmount) => lets (
        () => timeUp ?
          [timeUpText, timeUpButtonTextNext, curScore] :
          [timeRemainingText, timeRemainingButtonTextNext, bonusAmount],
        ([text, buttonText, scoreToShow]) => scoreToShow | loy,
        ([text, buttonText, scoreToShow], loyForm) => [buttonText, text | map (
          xReplace (/__POINTS__/, scoreToShow)
          >> xReplace (/__LOY__/, loyForm)
        )],
        (_1, _2, [buttonText, text]) => <WrapperS>
          <GameHeader/>
          <Text items={text}/>
          <ButtonWrapperS>
            <Button
              onClick={advance}
              size='medium'
            >
              {buttonText}
            </Button>
          </ButtonWrapperS>
        </WrapperS>,
      ),
    )
  }
)

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  bonusReceived: makeSelectBonusReceived (),
  timeUp: makeSelectTimeUp (),
  curScore: makeSelectCurScore (),
})

export default ScreenBonus
  | connect       (mapStateToProps, mapDispatchToProps)
