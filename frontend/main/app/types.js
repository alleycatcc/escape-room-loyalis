defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  noop, id, always,
  T, F, tap,
  concat, map,
  defaultTo,
  die, concatTo, precatTo,
  recurry, invoke, whenNil,
  prop,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, Nothing, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, iwarn, ierror, toString, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'

import {
  pickleMaybe, unpickleMaybe,
  pickleTaggedSum, unpickleTaggedSum,
} from './pickle/pickleTypes'

import config from './config'
import { spread, untag, } from './common'

const { configGet, configGetOr, configGets, configGetOrDie, } = config | configure.init

const Scn = daggy.taggedSum ('Scn', {
  ScnLogin: [],
  ScnText: [],
  ScnVideo: [],
  ScnTextBeforeGame: [],
  ScnGame: [],
  ScnTextBetweenGames: [],
  ScnGameOver: [],
  ScnGameBonus: [],
  ScnGameVideo: [],
  ScnGameTeamname: [],
  ScnTakePhoto: [],
  ScnEnd: [],
  ScnGameText: [],
})

const { ScnLogin, ScnText, ScnVideo, ScnGame, ScnTextBetweenGames, ScnTextBeforeGame, ScnGameOver, ScnGameBonus, ScnGameVideo, ScnGameTeamname, ScnTakePhoto, ScnEnd, ScnGameText, } = Scn

export { ScnLogin, ScnText, ScnVideo, ScnGame, ScnTextBeforeGame, ScnTextBetweenGames, ScnGameOver, ScnGameBonus, ScnGameVideo, ScnGameTeamname, ScnTakePhoto, ScnEnd,  ScnGameText,}

// --- @todo tap >> whenNil ...

// :: 'type' -> Scn
export const toScn = invoke (() => {
  const o = {
    login: ScnLogin,
    text: ScnText,
    video: ScnVideo,
    game: ScnGame,
    'text-between-games': ScnTextBetweenGames,
    'text-before-game': ScnTextBeforeGame,
    'game-over': ScnGameOver,
    'game-video': ScnGameVideo,
    'game-bonus': ScnGameBonus,
    'game-teamname': ScnGameTeamname,
    'take-photo': ScnTakePhoto,
    'game-text': ScnGameText,
    end: ScnEnd,
  }
  return (type) => o [type] | tap (whenNil (() => die ('Bad type string: ' + type)))
})

export const isScnGame = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: F,
  ScnGame: T,
  ScnTextBetweenGames: F,
  ScnGameOver: F,
  ScnGameBonus: F,
  ScnGameVideo: F,
  ScnGameTeamname: F,
  ScnTakePhoto: F,
  ScnEnd: F,
  ScnGameText: F,
})

export const isScnTextBetweenGames = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: F,
  ScnGame: F,
  ScnTextBetweenGames: T,
  ScnGameOver: F,
  ScnGameBonus: F,
  ScnGameVideo: F,
  ScnGameTeamname: F,
  ScnTakePhoto: F,
  ScnEnd: F,
  ScnGameText: F,
})

export const isScnGameTeamname = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: F,
  ScnGame: F,
  ScnTextBetweenGames: F,
  ScnGameOver: F,
  ScnGameBonus: F,
  ScnGameVideo: F,
  ScnGameTeamname: T,
  ScnTakePhoto: F,
  ScnEnd: F,
  ScnGameText: F,
})

export const isScnTextBeforeGame = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: T,
  ScnGame: F,
  ScnTextBetweenGames: F,
  ScnGameOver: F,
  ScnGameBonus: F,
  ScnGameVideo: F,
  ScnGameTeamname: F,
  ScnTakePhoto: F,
  ScnEnd: F,
  ScnGameText: F,
})

export const isScnGameBonus = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: F,
  ScnGame: F,
  ScnTextBetweenGames: F,
  ScnGameOver: F,
  ScnGameBonus: T,
  ScnGameVideo: F,
  ScnGameTeamname: F,
  ScnTakePhoto: F,
  ScnEnd: F,
  ScnGameText: F,
})

export const isScnTakePhoto = cata ({
  ScnLogin: F,
  ScnText: F,
  ScnVideo: F,
  ScnTextBeforeGame: F,
  ScnGame: F,
  ScnTextBetweenGames: F,
  ScnGameOver: F,
  ScnGameBonus: F,
  ScnGameVideo: F,
  ScnGameTeamname: F,
  ScnTakePhoto: T,
  ScnEnd: F,
  ScnGameText: F,
})

const Password = daggy.taggedSum ('Password', {
  PasswordNone: [],
  PasswordCorrect: [],
  PasswordIncorrect: [],
})

const { PasswordNone, PasswordCorrect, PasswordIncorrect, } = Password
export { PasswordNone, PasswordCorrect, PasswordIncorrect, }

export const picklePassword = pickleTaggedSum ({
  PasswordNone: 0,
  PasswordCorrect: 1,
  PasswordIncorrect: 2,
})

export const unpicklePassword = unpickleTaggedSum ([
  [0, PasswordNone | always],
  [1, PasswordCorrect | always],
  [2, PasswordIncorrect | always],
])

export const passwordIncorrect = cata ({
  PasswordNone: F,
  PasswordCorrect: F,
  PasswordIncorrect: T,
})

export const GameInfo = daggy.tagged ('GameInfo', [
  'password', 'hints',
])

GameInfo.prototype.untag = function (f) {
  const { password, hints, } = this
  return f (password, hints)
}

export const gameInfoPassword = 'password' | prop

export const Hint = daggy.tagged ('Hint', [
  'category',
  'text',
  'imageMb',
])

Hint.prototype.untag = function (f) {
  const { category, text, imageMb, } = this
  return f (category, text, imageMb)
}

export const pickleHint = untag (
  (category, text, imageMb) => [category, text, imageMb | pickleMaybe],
)

export const unpickleHint = spread (
  (category, text, imageMb) => Hint (category, text, imageMb | unpickleMaybe),
)

const CountdownSeverity = daggy.taggedSum ('CountdownSeverity', {
  CountdownSeverityNormal: [],
  CountdownSeverityLight: [],
  CountdownSeverityMedium: [],
  CountdownSeveritySevere: [],
})

CountdownSeverity.prototype.fold = function (f, g, h, i) {
  return this | cata ({
    CountdownSeverityNormal: f,
    CountdownSeverityLight: g,
    CountdownSeverityMedium: h,
    CountdownSeveritySevere: i,
  })
}

export const isSeverityNormal = cata ({
  CountdownSeverityNormal: T,
  CountdownSeverityLight: F,
  CountdownSeverityMedium: F,
  CountdownSeveritySevere: F,
})

const { CountdownSeverityNormal, CountdownSeverityLight, CountdownSeverityMedium, CountdownSeveritySevere, } = CountdownSeverity
export { CountdownSeverityNormal, CountdownSeverityLight, CountdownSeverityMedium, CountdownSeveritySevere, }
