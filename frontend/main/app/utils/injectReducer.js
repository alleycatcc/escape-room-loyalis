defineBinaryOperator ('|',  (...args) => pipe (...args))
defineBinaryOperator ('<<', (...args) => compose (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lets, sprintf1,
  deconstruct2,
} from 'stick-js/es'

import React from 'react'
import { ReactReduxContext, } from 'react-redux'
import hoistNonReactStatics from 'hoist-non-react-statics'

import getInjectors from './reducerInjectors'

export default ({ key, reducer, }) => deconstruct2 (({ displayName, name, }) =>
  WrappedComponent => lets (
    () => class ReducerInjector extends React.Component {
      // --- why is this necessary?
      static WrappedComponent = WrappedComponent

      static contextType = ReactReduxContext
      static displayName = (displayName || name || '[unknown]') | sprintf1 ('withReducer (%s)')

      constructor (props, context) {
        super (props, context)
        getInjectors (context.store).injectReducer (key, reducer)
      }

      render () {
        const { props, context: { store, }} = this
        // --- @experimental inject the entire store as a prop, useful for testing some functionality.
        // --- generally this should be done with a selector.
        // --- note that this doesn't cause rerenders when the store is updated because it's
        // actually slices of the store which get updated.
        return <WrappedComponent {...props} __store__={store}/>
      }
    },
    component => hoistNonReactStatics (component, WrappedComponent),
  )
)
