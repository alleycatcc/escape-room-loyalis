#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

go () {
    local i
    for i in *_example.py; do
        cmd ln -sf "$i" "${i/_example}"
    done
}; go
