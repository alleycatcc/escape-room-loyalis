# -*- coding: utf-8 -*-

import proj_paths # noqa

from enum import Enum
import re

from acatpy.util import pathjoin

def get(rootpath):
    web_path = pathjoin(rootpath, 'web')
    web_htdocs_path = pathjoin(web_path, 'htdocs')
    web_main_htdocs_path = pathjoin(web_htdocs_path, 'main')

    lib_path = pathjoin(rootpath, 'lib')
    lib_grne_path = pathjoin(lib_path, 'grne')
    frontend_src_path = pathjoin(rootpath, 'frontend')

    return {
        'web_main_build_path_tst': pathjoin(web_main_htdocs_path, 'tst', 'build'),
        'web_main_build_path_acc': pathjoin(web_main_htdocs_path, 'acc', 'build'),
        'web_main_build_path_prd': pathjoin(web_main_htdocs_path, 'prd', 'build'),
        'web_main_frontend_src_path': pathjoin(frontend_src_path, 'main'),
    }
