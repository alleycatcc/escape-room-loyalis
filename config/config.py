# -*- coding: utf-8 -*-

from functools import namedtuple
import os.path as path

from acatpy.util import pathparent, pathjoin

import config_build
import config_local
import config_main
import config_user

def get(config_script=None):
    Conf = namedtuple('Conf', [
        'main', 'local', 'user', 'build', 'script',
    ])

    # --- use the location of this config file as the starting point for calculating everything
    # else.
    rootpath = pathparent(path.dirname(__file__))
    sources_path = pathjoin(rootpath, 'sources')

    return Conf(
        config_main.get(rootpath),
        config_local.get(sources_path),
        config_user.get(),
        config_build.get(),
        config_script,
    )
