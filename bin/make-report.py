#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import argparse
from base64 import b64decode
from csv import writer as csv_writer
from functools import namedtuple
from json import loads
import os
import re
from time import gmtime

from adt import Case, adt

from acatpy.colors import yellow, green, magenta, cyan
from acatpy.io.io import die, mkdir, cmd
from acatpy.io.speak import warn, info, iwarn, bright_red
from acatpy.simple_table import Table
from acatpy.util import pathjoin

from escape.util.sqlite3 import db_open, sql_execute

'''
data DbSpec = DbPath { path :: String }
            | DbEnv { env :: String }
'''

DbPath = namedtuple('DbPath', ('path'))
DbEnv = namedtuple('DbEnv', ('env'))

@adt
class DbSpec:
    PATH: Case[DbPath]
    DROPLET_ENV: Case[DbEnv]

db_name = 'main.db'
droplet_host = 'escape'

def init(output_dir, db_spec):
    mkdir(output_dir)
    if not dir_exists_and_writeable(output_dir):
        die("Bad dir: %s" % bright_red(output_dir))
    # --- db_spec | cata ({ ... })
    db_path = db_spec.match(
        path=lambda p: p.path,
        droplet_env=lambda e: pathjoin(output_dir, download_from_droplet(output_dir, e.env)),
    )
    go(output_dir, db_path)

def go(output_dir, db_path):
    err, db = db_open(db_path, read_only=True)
    if err:
        die('Unable to open db %s: %s' % (bright_red(db_path), err))
    out_csv = pathjoin(output_dir, 'report.csv')
    out_txt = pathjoin(output_dir, 'report.txt')
    out_hints_csv = pathjoin(output_dir, 'report-hints.csv')
    out_hints_txt = pathjoin(output_dir, 'report-hints.txt')
     # open(out_hints_csv, 'w') as fh_hints_csv, \
    with open(out_csv, 'w') as fh_csv, \
         open(out_txt, 'w') as fh_txt, \
         open(out_hints_txt, 'w') as fh_hints_txt:
        # report(db, output_dir, fh_csv, fh_txt, fh_hints_csv, fh_hints_txt)
        report(db, output_dir, fh_csv, fh_txt, fh_hints_txt)
    # for x in out_csv, out_txt, out_hints_csv, out_hints_txt:
    for x in out_csv, out_txt, out_hints_txt:
        info('[%s] %s' % (green('write-report'), x))
def download_from_droplet(output_dir, env):
    info('[%s] from droplet env: %s' % (magenta('download'), yellow(env)))
    out = pathjoin(output_dir, db_name)
    cmd(
        ('scp', '%s:/var/local/escape-db/%s/main.db' % (droplet_host, env), out),
        die=True,
    )
    return db_name

def report(db, output_dir, fh_csv, fh_txt, fh_hints_txt):
    rows = sql_execute(
        db,
        '''
            select uid, time_started, hints, team_name, photo, time_elapsed, score from main
            where score is not null
        ''',
        (),
        throw=True,
    )
    fields = (
        ('team name', 30),
        ('time started', 20),
        ('time elapsed', 20),
        ('score', 10),
    )
    table = Table(fields=fields, fh_output=fh_txt)
    csv = csv_writer(fh_csv)
    for uid, time_started, hints, team_name, photo, time_elapsed, score in rows:
        team_name = team_name or '(--no team name--)'
        time_started_s = format_time_started(time_started) if time_started else '[unknown time]'
        score_s = default_to(score, '[error]', format_int)
        time_elapsed_s = default_to(time_elapsed, '[error]', lambda t: format_int(int(t / 1000.0)))
        table.row(team_name, time_started_s, time_elapsed_s, score_s)
        csv.writerow((team_name, time_started_s, time_elapsed_s, score_s))
        hints_obj = loads(hints)
        label = '%s - %s' % (time_started_s, team_name)
        if photo is not None:
            export_image(output_dir, label, photo)
        report_hints(fh_hints_txt, label, hints_obj)

def report_hints(fh_hints_txt, label, hints_obj):
    fh_hints_txt.write(label)
    fh_hints_txt.write('\n')
    fields = (
        ('game #', 7),
        ('category', 45),
        ('text', 45),
    )
    table = Table(fields=fields, fh_output=fh_hints_txt)
    for game_idx, hints_for_game in hints_obj.items():
        game_idx = str(1 + int(game_idx))
        for hint in hints_for_game:
            category = hint['category']
            text = '\n'.join(hint['text'])
            table.row(game_idx, category, text)
    fh_hints_txt.write('\n')
    # print('hints %s' % hints_obj)

def export_image(output_dir, basename, photo):
    photo = re.sub(r'^data:image/jpeg;base64,', '', photo)
    imgdata = b64decode(photo)
    filename = pathjoin(output_dir, '%s.jpg' % basename)
    with open(filename, 'wb') as fh:
        fh.write(imgdata)
        fh.write(b'\n')
    info('[%s] %s' % (cyan('write-image'), filename))

def format_time_started(epoch):
    year, mon, mday, hour, mins, sec, *_ = gmtime(epoch)
    return '%s-%02d-%02d %02d:%02d:%02d' % (year, mon, mday, hour, mins, sec)

def format_int(score):
    s = chunks(reverse_str(str(score)), 3)
    return reverse_str('.'.join(s))

# ------

def ident(x):
    return x

def default_to(x, d, f=ident):
    return d if x is None else f(x)

def reverse_str(s):
    return s[len(s)::-1]

# --- yields partial chunk at the end if not evenly divisible.
def chunks(xs, n_at_a_time):
    for i in range(0, len(xs), n_at_a_time):
        j = i + n_at_a_time
        yield xs[i:j]

def dir_exists_and_writeable(the_dir, **kw):
    return os.path.isdir(the_dir) and exists_and_writeable(the_dir, extra_flags=os.X_OK, **kw)

def exists_and_writeable(the_path, extra_flags=0, pref=None):
    ok = os.access(the_path, os.W_OK | extra_flags)
    if not ok:
        pref = pref + ' ' if pref else ''
        warn("%s%s doesn't exist or is not writeable" % (pref, bright_red(the_path)))
    return ok

# ------

def args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', metavar='output-dir', type=str,
        required=True,
        help='a directory for output; will be created if necessary',
    )
    db_group = parser.add_mutually_exclusive_group(
        required=True,
    )
    db_group.add_argument(
        '-d', metavar='db-path', type=str,
        help='path to db file',
    )
    db_group.add_argument(
        '-e', metavar='droplet-env', type=str,
        help='droplet environment (tst, acc, or prd) to download db from',
    )
    args = parser.parse_args()
    db_spec = None
    if args.d is not None:
        db_spec = DbSpec.PATH(DbPath(args.d))
    elif args.e is not None:
        db_spec = DbSpec.DROPLET_ENV(DbEnv(args.e))
    return args.o, db_spec


init(*args())
