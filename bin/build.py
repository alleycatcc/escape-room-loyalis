#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import os.path as path

from acatpy.io.speak import not_ok as speak_not_ok, ok as speak_ok, infon, info
from acatpy.io.io import die, cmd, cpa, rmdir
from acatpy.colors import red, yellow, bright_red
from acatpy.util import pathjoin

from config import get as config_get

def do_cmd(*args, **kw):
    kw['die'] = True
    cmd(*args, **kw)

def make_frontend_main(*args):
    return make_frontend_x(*args)

def make_frontend_x(env, srcpath, buildpath):
    do_cmd(['yarn'], cwd=srcpath)
    step = 'build-%s' % env
    do_cmd(['yarn run %s' % step], cwd=srcpath, shell=True)
    if path.exists(buildpath):
        rmdir(buildpath)
    cpa(pathjoin(srcpath, 'build'), buildpath)
    return True

def do_step(do_it, desc, run, pre=None):
    if not do_it:
        return info('[skip] %s' % bright_red(desc))
    info('[run] %s' % yellow(desc))
    if pre: pre()
    ok = run()
    if not ok: die('Failed at %s' % red(desc))

def go():
    config = config_get()

    do_step(
        config.build['do_frontend_main_tst'],
        'make-frontend-main-tst',
        lambda: make_frontend_main(
            'tst-optimised',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_tst'],
        ),
    )
    do_step(
        config.build['do_frontend_main_acc'],
        'make-frontend-main-acc',
        lambda: make_frontend_main(
            'acc',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_acc'],
        ),
    )
    do_step(
        config.build['do_frontend_main_prd'],
        'make-frontend-main-prd',
        lambda: make_frontend_main(
            'prd',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_prd'],
        ),
    )

    info('All done.')


go()
