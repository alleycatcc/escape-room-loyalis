# -*- coding: utf-8 -*-

from flask import jsonify

from escape.main import ping, user_init, hints_store, game_info_store, team_info_store
from escape.server.util import request_body_json, make_router, fail

def define_main_route(app, runtime_config, prefix):
    router = make_router(app, prefix, methods=['POST'])
    db = runtime_config.db

    @router('/ping', methods=['GET'])
    def route_main_ping():
        response_data = {'data': {
            'error': None,
            'pong': ping(),
        }}
        return jsonify(response_data)

    @router('/ping-echo', methods=['POST'])
    def route_main_ping_echo():
        request_body = request_body_json()

        response_data = {'data': {
            'error': None,
            'pong': request_body['message'],
        }}
        return jsonify(response_data)

    @router('/user-init', methods=['POST'])
    def route_main_user_init():
        uid = user_init(db)
        response_data = {'data': {
            'error': None,
            'uid': uid,
        }}
        return jsonify(response_data)

    @router('/hints-store', methods=['POST'])
    def route_main_hints_store():
        request_body = request_body_json()
        uid = request_body['uid']
        game_idx = request_body['game-idx']
        hints = request_body['hints']
        hints_store(db, uid, game_idx, hints)
        response_data = {'data': {
            'error': None,
        }}
        return jsonify(response_data)

    @router('/game-info-store', methods=['POST'])
    def route_main_game_info_store():
        request_body = request_body_json()
        uid = request_body['uid']
        score = request_body['score']
        time_elapsed = request_body['time-elapsed']
        game_info_store(db, uid, score, time_elapsed)
        response_data = {'data': {
            'error': None,
        }}
        return jsonify(response_data)

    @router('/team-info-store', methods=['POST'])
    def route_main_team_info_store():
        request_body = request_body_json()
        uid = request_body['uid']
        photo = request_body['photo']
        team_name = request_body['team-name']
        team_info_store(db, uid, photo, team_name)
        response_data = {'data': {
            'error': None,
        }}
        return jsonify(response_data)
