# -*- coding: utf-8 -*-

import json
from time import time
from uuid import uuid4 as make_uuid

from escape.util.sqlite3 import (
    db_init, table_add, index_add,
    sql_execute as _sql_execute,
    sql_execute_fetchone as _sql_execute_fetchone,
)

from config import get as config_get

def sql_execute(*args, **kw):
    return _sql_execute(*args, **kw, throw=True)

def sql_execute_fetchone(*args, **kw):
    return _sql_execute_fetchone(*args, **kw, throw=True)

def ping():
    return time()

def init():
    config = config_get()
    db, (created,) = db_init(config.local['db_path'], rm=False, throw=True)
    if created:
        init_schema(db)
    return db

def init_schema(db):
    table_add(
        db,
        'main',
        (
            'id integer not null primary key',
            # --- uuid as string.
            'uid string not null',
            # --- epoch ms
            'time_started integer',
            # --- json list.
            'hints string',
            'team_name string',
            # --- base64-encoded string.
            'photo string',
            # --- ms
            'time_elapsed integer',
            'score integer',
        )
    )

    index_add(db, 'main', 'uid')

def user_init(db):
    uuid = str(make_uuid())
    sql = 'insert into main (uid, time_started, hints) values (?, ?, ?)'
    sql_execute(db, sql, (uuid, time(), '{}'))
    return uuid

def hints_store(db, uid, game_idx, hints_to_store):
    (hints_json,) = sql_execute_fetchone(
        db,
        'select hints from main where uid = ?',
        (uid,)
    )
    hints = json.loads(hints_json)
    hints[game_idx] = hints_to_store
    hints_json_new = json.dumps(hints)
    sql_execute(
        db,
        'update main set hints=? where uid=?',
        (hints_json_new, uid),
    )

def game_info_store(db, uid, score, time_elapsed):
    sql_execute(
        db,
        'update main set time_elapsed=?, score=? where uid=?',
        (time_elapsed, score, uid),
    )

def team_info_store(db, uid, photo, team_name):
    sql_execute(
        db,
        'update main set photo=?, team_name=? where uid=?',
        (photo, team_name, uid),
    )
