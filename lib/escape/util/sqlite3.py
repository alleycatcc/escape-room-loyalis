# -*- coding: utf-8 -*-

from os import path
import sqlite3

from acatpy.colors import green, yellow, bright_red
from acatpy.io.io import rm
from acatpy.io.speak import info, error
from acatpy.util import alias_kw

# --- read_only also means, don't make the file if it doesn't exist; note that it's up to you to
# make sure the file exists in this case because the driver doesn't complain.
# --- python3 sqlite3 uses transactions by default.

def db_open(db_path, read_only=False, throw=False):
    if read_only:
        uri = 'file:%s?mode=ro' % db_path
        args = [uri]
        kw = {'uri': True}
    else:
        args = [db_path]
        kw = {}
    db = None
    try:
        db = sqlite3.connect(*args, **kw)
        return db if throw else (None, db)
    except sqlite3.OperationalError as e:
        if throw:
            raise e
        else:
            return e, None

@alias_kw(rmarg='rm')
def db_init(db_path, rmarg=True, throw=False):
    exists = path.exists(db_path)
    if exists:
        if rmarg:
            info('Removing db %s' % db_path)
            rm(db_path)
            info('Creating db %s.' % green(db_path))
            created = True
        else:
            info('Updating db %s' % yellow(db_path))
            created = False
    else:
        info('Creating db %s.' % green(db_path))
        created = True
    err, db = db_open(db_path, throw=False)
    if err:
        error("Unable to open db %s: %s" % (bright_red(db_path), err))
        if throw:
            raise err
    return db, (created,)

def table_add(db, name, column_names, extra=None):
    column_names_str = ', '.join(column_names)
    sql = 'create table %s (%s)' % (name, column_names_str)
    sql = '%s %s' % (sql, extra) if extra is not None else sql
    db.execute(sql)

def index_add(db, table_name, column_name, index_name=None, unique=False, extra=None):
    index_name = index_name or 'i_%s' % table_name
    i = 'unique index' if unique else 'index'
    sql = 'create %s %s on %s(%s)' % (i, index_name, table_name, column_name)
    sql = '%s %s' % (sql, extra) if extra is not None else sql
    db.execute(sql)

def sql_execute(db, sql, bindings, cur_method='fetchall', throw=False, quiet=False):
    with db:
        try:
            cur = db.execute(sql, bindings)
            res = getattr(cur, cur_method)()
            if throw:
                return res
            return None, res
        except sqlite3.OperationalError as e:
            if not quiet:
                error("Error: %s (sql=%s, bindings=%s)" % (e, sql, list(bindings)))
            if throw:
                raise e
            return e, None

def sql_execute_fetchone(*args, **kw):
    return sql_execute(*args, **kw, cur_method='fetchone')
